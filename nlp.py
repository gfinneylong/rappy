from sql_interface import *
from util import *
import pickle
import ast
import nltk

alphabet = 'abcdefghijklmnopqrstuvwxyz\'-'

cmudict = nltk.corpus.cmudict.dict()


class ngram_tree_node:
    def __init__(self, char, depth, parent):
        self.char = char
        self.depth = depth
        self.parent = parent
        self.count = 0
        self.children = {}

    def make_children(self):
        for letter in alphabet:
            self.children[letter] = ngram_tree_node(letter, self.depth + 1, self)

    def get_ngram(self, seperator=''):
        '''
        Ascends through tree up to root, walking through the ngram which terminates with this node
        :return: A string, corresponding the ngram represented (and terminated) by this node
        '''
        def get_ngram_helper(cur_node, cur_ngram):
            if cur_node.parent is None: # Called on root (don't do this!)
                return ''
            if cur_node.parent.parent is None: # if parent is root
                return cur_node.char + seperator + cur_ngram
            else:
                return get_ngram_helper(cur_node.parent, cur_node.char + seperator + cur_ngram)

        return get_ngram_helper(self, '')[:-1] # Removing terminating period that exists because start with ngram=''

    def __str__(self):
        return '(' + str(self.char) + ' : ' + str(self.count) + ')'

    def __lt__(self, other_node):
        return self.char < other_node.char

    __repr__ = __str__


class ngram_tree:
    # Root is depth -1, first nodes are at depth 0, and have ngram length of 1
    # Their children are at depth 1, and have ngram length of 2
    def __init__(self, max_depth, cmutree=False):
        self.root = ngram_tree_node('ROOT', 0, None) # Keep -1 for get_nodes_at_depth
        self.max_depth = max_depth
        self.cmu = cmutree

    def print(self, max_print_depth=sys.maxsize, sort_by_count=False):
        def print_tree_helper(head, indent=0, root=False, max_print_depth=sys.maxsize, sort_by_count=False):  # Handles nodes instead of tree
            if indent <= max_print_depth:
                [print('-', end='') for i in range(indent - 1)]
                if indent:
                    print('+', end='')
                print(head)
                if sort_by_count:
                    for child in sorted(head.children.values(), key=lambda x: x.count, reverse=True):
                        print_tree_helper(child, indent=indent + 1, max_print_depth=max_print_depth, sort_by_count=sort_by_count)
                else:
                    for child in sorted(head.children.values()):
                        print_tree_helper(child, indent=indent + 1, max_print_depth=max_print_depth, sort_by_count=sort_by_count)

        print_tree_helper(self.root, root=True, max_print_depth=max_print_depth, sort_by_count=sort_by_count)

    def add_word(self, word):
        for substr_len in range(1, len(word) + 1): # last length with be the full length of the word
            for offset in range(0, len(word) - substr_len + 1):
                cur_word = word[offset:offset + substr_len]
                cur_node = self.root
                if len(cur_word[:self.max_depth]) > 1:
                    for letter_index, letter in enumerate(cur_word[:self.max_depth]):
                        if letter not in cur_node.children:
                            cur_node.children[letter] = ngram_tree_node(letter, cur_node.depth + 1, cur_node)
                        cur_node = cur_node.children[letter]
                        if letter_index == len(cur_word[:self.max_depth]) - 1: # NOTE: Only update count for a node from the last letter of the word
                            cur_node.count += 1

    def get_nodes_at_depth(self, depth):
        '''
        :param depth:
        :return:
        '''

        def get_nodes_at_depth_helper(cur_node, depth):
            if cur_node.depth == depth - 1: # Parent of correct depth
                return [child for child in cur_node.children.values()]
            else:
                return [result_node for child in cur_node.children.values() for result_node in get_nodes_at_depth_helper(child, depth)]

        return get_nodes_at_depth_helper(self.root, depth)

    def get_ngrams_of_length(self, length, sort_by_count=True, seperator=''):
        if length == 0:
            return []
        nodes_at_depth = self.get_nodes_at_depth(length)


        if sort_by_count:
            return sorted([(node.get_ngram(seperator=seperator), node.count) for node in nodes_at_depth], key=lambda x: x[1], reverse=True)
        else:
            return [(node.get_ngram(seperator=seperator), node.count) for node in nodes_at_depth]

    def find_n_most_common_ngrams(self, find_n_most_common, min_length=2, seperator=''):
        # Create a tree using dicts to house ngrams
        # Each node needs a 'count' key, which refers to how often that ngram has appeared
        # Will use all words in Words to do this
        # Note find_n_most_common is an int or a list of ints

        if type(find_n_most_common) is int:
            return [self.get_ngrams_of_length(cur_len, seperator=seperator)[:find_n_most_common] for cur_len in range(min_length, self.max_depth + 1)] # +1 to include max_depth
        else:
            assert(type(find_n_most_common) is list)
            assert(len(find_n_most_common) == self.max_depth - min_length + 1)
            return [self.get_ngrams_of_length(cur_len, seperator=seperator)[:find_n_most_common[len_index]] for len_index, cur_len in enumerate(range(min_length, self.max_depth + 1))]


    def add_n_most_common_ngrams(self, find_n_most_common, min_length=2, offset=0, column_type='ngram', seperator=''):
        # Note find_n_most_common is an int or a list of ints
        ngrams_by_size = self.find_n_most_common_ngrams(find_n_most_common, min_length=min_length, seperator=seperator)
        load_cursor_with_words()
        words = [word[0] for word in sql_cursor]
        print("Adding columns to Words table for ngrams of type:" + column_type + " with lengths [" + str(min_length) + ',' + str(self.max_depth) + "], start_time: " + str(strftime("%H:%M:%S")))
        for ngram_list in ngrams_by_size:
            for ngram_index, (ngram, ngram_count) in enumerate(ngram_list):
                ngram = ngram.lower()
                add_feature_column_to_words(ngram, column_type, 'TINYINT', output=True)
        save_database()
        print("Populating %s cells" % column_type, flush=True)
        print_n_times = 100
        print_every_n = int(len(words) / print_n_times)
        column_type = column_type.lower()

        for index, cur_word in enumerate(words):
            if index % print_every_n == 0: #Removed for debug #and index != 0:
                print(" Adding " + str(column_type) + "s for word: \"" + (str(cur_word)+'\"').ljust(15) + ' = #' + str(index).rjust(2) + '/' + str(len(words)) + ' cur_time: ' + str(get_time()))
            exec_str = 'UPDATE Words SET '
            for length, ngram_list in enumerate(ngrams_by_size[max(0, offset-min_length):], max(min_length, offset)):
                for ngram_index, (ngram, ngram_count) in enumerate(ngram_list):
                    ngram = ngram.lower()
                    if self.cmu:
                        exec_str += '`f_' + column_type + '_' + ngram + '`=' + str('_'.join(cmudict[cur_word][0]).lower().count(ngram)) + ', '
                    else:
                        exec_str += '`f_' + column_type + '_' + ngram + '`=' + str(cur_word.count(ngram.replace("_", ""))) + ', ' # Todo faster alt to replacing..
            exec_str = exec_str[:-2] + ' WHERE word="' + str(cur_word) + '"'
            execute(exec_str, [], output=False)
            save_database(output=False)
        save_database(output=True) # Just so user can see output


def make_tree_all_words(max_length=6):
    print("Creating ngram tree with all words in Words table, max ngram length = " + str(max_length))
    my_tree = ngram_tree(max_length)
    total_words = get_words_count()
    word_num = 0
    load_cursor_with_words()
    update_every_n = int(total_words / 5)

    while True:
        word_num += 1
        if word_num % update_every_n == 0:
            print(' ' + str(word_num) + ' / ' + str(total_words) + ' words in tree', flush=True)
        add_word = next_item_from_cursor()
        if add_word is None:
            break
        else:
            my_tree.add_word(add_word)
    return my_tree


def make_tree_all_cmudict(max_length=6):
    print("Creating ngram tree with all cmudict representations of words in Words table, max ngram length = " + str(max_length))
    my_tree = ngram_tree(max_length, cmutree=True)
    total_words = get_words_count()
    update_every_n = int(total_words / 5)
    execute("SELECT v_cmurep FROM Words", [], output=False)
    word_num = 0
    while True:
        word_num += 1
        if word_num % update_every_n == 0:
            print(' ' + str(word_num) + ' / ' + str(total_words) + ' words in tree', flush=True)
        add_word = next_item_from_cursor()
        if add_word is None:
            break
        else:
            my_tree.add_word(add_word.split('_'))
    return my_tree


def make_tree_all_metaphones(max_length=6):
    print("Creating ngram tree with all metaphones in Words table, max ngram length = " + str(max_length))
    my_tree = ngram_tree(max_length)
    total_words = get_words_count()
    word_num = 0
    execute("SELECT v_metaphone FROM Words", [], output=False)
    update_every_n = int(total_words / 5)
    while True:
        word_num += 1
        if word_num % update_every_n == 0:
            print(' ' + str(word_num) + ' / ' + str(total_words) + ' words in tree', flush=True)
        add_word = next_item_from_cursor()
        if add_word is None:
            break
        else:
            my_tree.add_word(add_word)
    return my_tree
