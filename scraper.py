#import beautifulsoup4
import urllib.request
import glob

import re
import ast
from util import *
# Corpus of most common words: http://norvig.com/ngrams/count_1w.txt (1/3 million)
from sql_interface import *
from subprocess import call

def fetch_rhyming_words(word, require_scores=False, remove_multi_words=True):
    '''

    :param word: The word to search for rhymes to
    :param require_scores: All rhyming words must have a score, which describes the quality of the rhyme
    :param remove_multi_words: Remove matches that are two words, like 'kitten' against 'fit in'
    :return:
    '''
    f = urllib.request.urlopen("https://api.datamuse.com/words?rel_rhy=" + word)
    res = ast.literal_eval(f.read().decode("utf-8"))  # res is a list of dicts
    if require_scores:
        res = [dic for dic in res if 'score' in dic]
    if remove_multi_words:
        res = [dic for dic in res if ' ' not in dic['word']]
    return res



def scrape_from_datamuse(offset=0, save_frequency=500, scrape_via_scrapy=SCRAPE_VIA_SCRAPEY):
    call([SCRAPEY_DIR, "crawl", "cmu_spider"])
    # call([SCRAPEY_DIR, "crawl", "count1w_spider"])
        # Now process data which has been saved to ./Scraped/'WORD'.html

def process_data_from_datamuse(scrape_via_scrapy=SCRAPE_VIA_SCRAPEY, require_scores=False, remove_multi_words=True, update_every_n=10000):
    if not SCRAPE_VIA_SCRAPEY:
        print('Processing was handled at the time of scraping')
        pass
    else:
        total_files = len(glob.glob("scraped/*.html"))
        print("Now processing " + str(total_files) + " downloaded rhyme files")
        total_rhymes = 0
        too_long = []
        for file_no, filename in enumerate(glob.glob("scraped/*.html")):
            with open(filename) as file:
                cur_word = filename[filename.find('\\') + 1:-5]
                if len(cur_word) > MAX_WORD_LENGTH:
                    too_long.append(cur_word)
                else:
                    # add_word(cur_word, output=False) # Note removed because cmudict
                    contents_list = ast.literal_eval(file.read())
                    if require_scores:
                        contents_list = [dic for dic in contents_list if 'score' in dic]
                    if remove_multi_words:
                        contents_list = [dic for dic in contents_list if ' ' not in dic['word']]
                    total_rhymes += len(contents_list)
                    for rhyme_dict in contents_list:
                        add_rhyme(cur_word, rhyme_dict['word'], output=False)
            if file_no % update_every_n == 0 and file_no:
                print(" Added " + str(file_no) + '/' + str(total_files) + ' words which have ' + str(total_rhymes) + ' total rhymes. ', end='')
                save_database()
        print("Skipped " + str(len(too_long)) + " words due to their length (exceeds " + str(MAX_WORD_LENGTH) + ')')
        print("Skipped words: " + str(too_long))



