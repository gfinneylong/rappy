import mysql.connector
import sys
import os

# MYSQL
DB_NAME = 'Rappy_2'

mysql_config = {
    'user': 'root',
    'port': '3306',
    'database': '',
    'password': 'rappypassword',
    'host': '192.168.1.140'
}
OUTPUT_FROM_EXECUTE_ON_ERROR = True



# DIRECTORIES
PICKLE_DIR = "H:/Backups/Programming/Rappy_pickles/"


GRAPHIZ_DOT_LOCATION = "C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe"

SCRAPE_VIA_SCRAPEY = True # The alternativeis to use the slower but simpler scraper.py

SCRAPEY_DIR = "C:/Anaconda/envs/py3k/Scripts/scrapy"
SCRAPED_FILES_DIR = "C:/Users/gio/Documents/Programming/rappy/scraped/"

MAX_WORD_LENGTH = 25

# rappy.py flags:
REGEN_DB_WO_SCRAPING = False # Convenient way to delete then regenerate entire db without doing scraping (assumed to already have scraped files)

if not REGEN_DB_WO_SCRAPING:
    WIPE_DATA = False
    SCRAPE_RHYME_DATA = False  # Gets rhyme files for all words in cmudict, and saves to Scraped/
    POPULATE_WORDS_AND_REPS_AND_RHYMES = False  # Adds words to Words table, as well as cmu-representation. Populates whole Rhymes table
    REMOVE_WORDS_WITH_NO_RHYMES = False # Also removes the files of those who have no rhymes
    ADD_NORMAL_NGRAMS = False  # Creates ngram trees for words and CMU-representations, and adds most common
    ADD_CMUDIT_NGRAMS = False  # Creates ngram trees for words and CMU-representations, and adds most common
    RUN_INTERFACE = True  # Allows either loading or generating of interface
    LOAD_INTERFACE = False  # Only applicable if RUN_INTERFACE is True
else:
    #  NOTE do NOT modify these - they are for REGEN_DB_WO_SCRAPING
    WIPE_DATA = True
    SCRAPE_RHYME_DATA = False
    POPULATE_WORDS_AND_REPS_AND_RHYMES = True
    REMOVE_WORDS_WITH_NO_RHYMES = True
    ADD_NORMAL_NGRAMS = True
    ADD_CMUDIT_NGRAMS = True
    RUN_INTERFACE = False
    LOAD_INTERFACE = False
    # NOTE do NOT modify these - they are for REGEN_DB_WO_SCRAPING




# Specifics of adding ngrams
MAX_STD_NGRAM_LEN = 6
MIN_STD_NGRAM_LEN = 2 # Ideally 2, must be > 0
MAX_CMU_NGRAM_LEN = 6
MIN_CMU_NGRAM_LEN = 2 # Ideally 2, must be > 0


if DB_NAME == "Rappy_2": # HACK
    STD_NGRAM_COUNTS = [50, 45, 40, 25, 25] # Note that this can be an int, or a list of ints equal to max_len-min_len
    CMU_NGRAM_COUNTS = [50, 45, 40, 25, 25] # Note that this can be an int, or a list of ints equal to max_len-min_len
else:
    STD_NGRAM_COUNTS = 25  # Note that this can be an int, or a list of ints equal to max_len-min_len
    CMU_NGRAM_COUNTS = 25  # Note that this can be an int, or a list of ints equal to max_len-min_len

if type(STD_NGRAM_COUNTS) is list:
    assert(len(STD_NGRAM_COUNTS) == MAX_STD_NGRAM_LEN - MIN_STD_NGRAM_LEN + 1) # +1 to include both endpoints
if type(CMU_NGRAM_COUNTS) is list:
    assert(len(CMU_NGRAM_COUNTS) == MAX_CMU_NGRAM_LEN - MIN_CMU_NGRAM_LEN + 1)

# TODO add metaphone and use PCA to choose features (for k means)