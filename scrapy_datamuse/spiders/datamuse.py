import scrapy
import os
from sql_interface import *


def clean_text(text): # TODO move to util file
    '''
    Cleans a single isolated word's string representation, to a unified format
    :param text: The word to be cleaned
    :return: A str representing the cleaned word
    '''
    text = ''.join([char for char in text if char in 'ABCDEFGHIJKLNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-\''])
    return text.lower()


# class Count1w_Spider(scrapy.Spider):
#     name = "count1w_spider"
#     # Note removed when switching from corpora to cmudict
#
#
#     corpora_str = "C:/Users/gio/Documents/Programming/rappy/Corpora/count_1w.txt"
#     f = open(corpora_str, 'r')
#     num_lines = sum(1 for line in open(corpora_str))
#     print('The total number of lines is: ' + str(num_lines))
#     start_urls = []
#     base_url = 'https://api.datamuse.com/words?rel_rhy='
#     offset = 0 # HACK
#
#
#     for line_num, line in enumerate(f):
#         if line_num > offset:
#             word = clean_text(line[:line.find('\t')])
#             if len(word): # Make sure we still have a word!
#                 start_urls.append(base_url + word)
#     print("The total number of start_urls is " + str(len(start_urls)))
#
#     allowed_domains = ["api.datamuse.com"]
#     if not os.path.exists("C:/Users/gio/Documents/Programming/rappy/Scraped"):
#         os.makedirs("C:/Users/gio/Documents/Programming/rappy/Scraped")
#
#     def parse(self, response):
#         filename = "C:/Users/gio/Documents/Programming/rappy/Scraped/" + response.url[response.url.find('=') + 1:] + '.html'
#         with open(filename, 'wb') as f:
#            f.write(response.body)
#

class CMU_Spider(scrapy.Spider):
    name = "cmu_spider"
    all_words = get_all_words()
    num_lines = len(all_words)
    start_urls = []

    allowed_domains = ["api.datamuse.com"]
    base_url = 'https://api.datamuse.com/words?rel_rhy='
    if not os.path.exists("C:/Users/gio/Documents/Programming/rappy/scraped"):
        os.makedirs("C:/Users/gio/Documents/Programming/rappy/scraped")
    for word in all_words:
        start_urls.append(base_url + word)
    print("Total start urls: " + str(len(start_urls)))

    def parse(self, response):
        filename = "C:/Users/gio/Documents/Programming/rappy/scraped/" + response.url[
                                                                         response.url.find('=') + 1:] + '.html'
        with open(filename, 'wb') as f:
            f.write(response.body)