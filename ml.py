from sklearn import preprocessing as pp, tree
from sql_interface import *
import numpy as np
import scipy.sparse as sparse
from nlp import *
import subprocess
from util import *
import os
from sklearn.cluster import KMeans
import random
from sklearn.ensemble import RandomForestClassifier


def get_rhyme_pairs():
    w1 = ['cat', 'bee', 'limber', 'limp', 'gold', 'kite', 'bark', 'seven', 'dog', 'munch', 'went', 'lint', 'top', 'word']
    w2 = ['bat', 'tree','timber', 'pimp', 'sold', 'bite', 'dark', 'eleven','log', 'brunch','bent', 'mint', 'mop', 'bird']
    w1 += ['measure' , 'beer', 'near', 'milk', 'car', 'star', 'seed', 'tired', 'shorts', 'train',   'stare' ]
    w2 += ['treasure', 'tear', 'tier', 'silk', 'bar', 'far' , 'need', 'fired', 'sports', 'membrane', 'hare']
    w1 += ['wink', 'cape', 'lighter', 'key', 'butter', 'nose', 'boy', 'forgetful', 'bear', 'aware',  'prayer', 'unfair']
    w2 += ['sink', 'ape',  'fighter', 'flea', 'flutter','hose', 'toy', 'regretful','chair','despair', 'swear', 'ensnare']
    w1 += ['little' , 'heaven', 'declare', 'wear',  'lair', 'minh','roar', 'door', 'sprinkler', 'bearing']
    w2 += ['spittle', 'kevin',  'repair' , 'snare', 'care', 'fin', 'chore','war', 'ink', 'herring']
    assert len(w1) == len(w2)
    return w1, w2


def get_nonrhyme_pairs():
    w3 = ['stone', 'cup', 'cat', 'dog', 'horse', 'milk',  'tree', 'trip', 'computer', 'alien', 'tiny']
    w4 = ['child', 'sat', 'too', 'sit', 'mouse', 'soda',  'root', 'fall', 'program',  'trick', 'less']
    w3 += ['giant',  'devil', 'cup', 'left', 'right', 'police', 'zombie', 'scar', 'mountain', 'poll', 'election', 'microphone']
    w4 += ['little','violin', 'bat', 'up',   'down',  'around', 'invade', 'hunt', 'vast',     'vote', 'wasted',   'singer']
    w3 += ['mega', 'cheese', 'coffee', 'cup', 'poster', 'phone', 'seed', 'aluminum', 'funnel', 'monkey', 'orange', 'banana']
    w4 += ['house', 'knife', 'drink', 'milk', 'sauce', 'hat',  'speaker', 'tree',  'twig',   'pig',  'cable', 'purple']

    w3 += ['forest', 'stone', 'stone', 'cup', 'sit']
    w4 += ['random', 'sat', 'mild', 'mouse', 'cup']
    assert len(w3) == len(w4)
    return w3, w4


def combine_features(feature_arr1, feature_arr2):
    # TODO slow, takes 1.25 minutes to do 500 when making training set using this, ~7 seconds without
    combined_arr = np.zeros(feature_arr1.shape, dtype=feature_arr1.dtype)
    for row in range(feature_arr1.shape[0]):
        for col in range(feature_arr1.shape[1]):
            combined_arr[row][col] = int(feature_arr1[row][col] > 0 and feature_arr2[row][col] > 0)
    return combined_arr


# NOTE using below method to replace old style of looking up directly in feature matrix
# NOTE doing this will allow the addition of other features similar to combine features
# NOTE these features will be non-binary, and based entirely on the relations between two words
# TODO eventually use this to replace the if combine block in check_rhyme
def get_relationships(word1, word2):
    cmu1 = cmudict[word1][0]
    cmu2 = cmudict[word2][0]
    # rev_zip example:
    # a = 1,2,3
    # b = 4,5,6,7
    # list(rev_zip) = [(3, 7), (2, 6), (1, 5)]
    rev_cmu_zip = zip(reversed(cmu1), reversed(cmu2))
    cmus_same_from_back = 0 # This is important because in english, most rhymes are from the back
                            # This is probably the most important feature
    for i1, i2 in rev_cmu_zip:
        if i1 == i2:
            cmus_same_from_back += 1
        else:
            break

    fwd_zip = zip(cmu1, cmu2)
    cmus_same_from_front = 0
    for i1, i2 in fwd_zip:
        if i1 == i2:
            cmus_same_from_front += 1
        else:
            break
    cmu_set1 = set(cmu1)
    cmu_set2 = set(cmu2)
    same_cmu_count = len(cmu_set1 & cmu_set2) # Note that this doesn't include duplicates with either word
    different_cmu_count = len(cmu_set1 ^ cmu_set2)
    return np.array([[cmus_same_from_back, cmus_same_from_front, same_cmu_count, different_cmu_count]])
    # Double list to be [1,X] in shape instead of [X,]
    # TODO concat this
    # TODO use get method instead of direct lookup from feature matrix to accomidate this...


def check_rhyme(mla, feature_matrix, word_to_row, word1, word2, combine=True, output=True):
    if output:
        print("Checking rhyme between: " + str(word1) + ' & ' + str(word2))
    w1_arr = word_feature_vector_from_matrix(feature_matrix, word_to_row, word1)
    w2_arr = word_feature_vector_from_matrix(feature_matrix, word_to_row, word2)
    extra_features = get_relationships(word1, word2)

    if combine:
        return mla.predict(np.concatenate((w1_arr, w2_arr, combine_features(w1_arr, w2_arr), extra_features), axis=1))
    else:
        # return mla.predict(np.concatenate((w1_arr.flatten(), w2_arr.flatten(), extra_features), axis=0))
        return mla.predict(np.concatenate((w1_arr, w2_arr, extra_features), axis=1)) # OLD way


def check_km_rhyme(km, feature_matrix, word_to_row, word1, word2, output=False):
    if output:
        print("Checking KM rhyme between %s & %s" % (word1, word2))
    w1_arr = word_feature_vector_from_matrix(feature_matrix, word_to_row, word1)
    w2_arr = word_feature_vector_from_matrix(feature_matrix, word_to_row, word2)
    group_1 = km.predict(w1_arr)
    group_2 = km.predict(w2_arr)
    if group_1 == group_2:
        return True, group_1
    else:
        return group_1, group_2


def word_feature_vector_from_matrix(feature_matrix,  word_to_row, word):
    return feature_matrix[word_to_row[word]].toarray()


def save_feature_matrix(mat, filename="feature_matrix.pickle", binary=False):
    filename = format_matrix_filename(filename=filename, binary=binary)
    print("Saving feature matrix as:" + filename)
    pickle.dump({"matrix": mat}, open(filename, "wb"))


def load_feature_matrix(filename="feature_matrix.pickle", binary=False, output=True):
    filename = format_matrix_filename(filename=filename, binary=binary)
    if output:
        print("\nLoading feature matrix from: " + filename)
    return pickle.load(open(filename, "rb"))["matrix"]


def load_inputs(word_count, filename="dtree_ins_outs.pickle", combine=True):
    filename = format_dtree_training_filename(word_count, filename=filename, combine=combine)
    print("Loading training inputs from: " + str(filename))
    ins_outs = pickle.load(open(filename, "rb"))
    inputs = ins_outs["inputs"]
    outputs = ins_outs["outputs"]
    print("Length of inputs: " + str(with_commas(len(inputs))) + " Width of inputs (# features): " + str(inputs[0].shape[1]))
    return inputs, outputs


def load_km_inputs(word_count, filename="kmeans_ins.pickle"):
    filename = format_dtree_training_filename(word_count, filename=filename, combine=False)
    inputs = pickle.load(open(filename, "rb"))["inputs"]
    print("Length of inputs: " + str(with_commas(len(inputs))) + ", Width of inputs (# features): " + str(inputs[0].shape[1])) # CHECKME should be shape0??
    return inputs


def save_inputs(word_count, inputs, outputs, filename="dtree_ins_outs.pickle", combine=True):
    filename = format_dtree_training_filename(word_count, filename=filename, combine=combine)
    print("Saving training inputs as:%s, Length of inputs:%s Width of inputs (# features):%s" % (filename, with_commas(len(inputs)), inputs[0].shape[1]))
    pickle.dump({"inputs": inputs, "outputs": outputs}, open(filename, "wb"))


def save_km_inputs(word_count, inputs, filename="kmeans_ins.pickle"):
    filename = format_dtree_training_filename(word_count, filename=filename, combine=False)
    print("Saving kmeans training inputs as: " + str(filename))
    print("Length of inputs: " + str(len(inputs)))
    print("Width of inputs (# features): " + str(inputs[0].shape[1]))
    pickle.dump({"inputs": inputs}, open(filename, "wb"))


def gen_feature_matrix(binary=False):
    feature_names = get_feature_column_names()
    all_words = get_all_words()
    col_count = len(feature_names)
    row_count = get_words_count()
    matrix = sparse.lil_matrix((row_count, col_count), dtype=np.int8) # NOTE using little matrix instead of csr_matrix b/c constructing incrementally
    feature_to_col = {}
    word_to_row = {}
    for col_num, feature in enumerate(feature_names):
        feature_to_col[feature] = col_num
    for row_num, word in enumerate(all_words):
        word_to_row[word] = row_num
    load_feature_vectors()
    if binary:
        print("Populating binary feature matrix")
    else:
        print("Populating feature matrix")
    print_every_n = 25000
    for row_num, row in enumerate(sql_cursor):
        # NOTE CAN assign tuple to row
        if binary:
            matrix[row_num] = [i != 0 for i in row]
        else:
            matrix[row_num] = row
        if row_num % print_every_n == 0:
            print(" Row #" + str(row_num) + '/' + str(row_count))
    save_feature_matrix(matrix, binary=binary)
    return matrix


def gen_km_training_set(feature_matrix, words_to_use=10000): # Note no combining! Otherwise will smush the centers together..
    print_every_n = 1000
    feature_names = get_feature_column_names()
    all_words = get_all_words()
    feature_to_col = {}
    word_to_row = {}
    row_to_word = {}  # Calculate once here to save time in getting nonrhyming words
    training_input = []
    for col_num, feature in enumerate(feature_names):
        feature_to_col[feature] = col_num
    for row_num, word in enumerate(all_words):
        word_to_row[word] = row_num
        row_to_word[row_num] = word

    words_to_use = min(len(all_words), words_to_use)
    skip = int(len(all_words) / words_to_use)
    print('Generating kmeans training set, words limited to: ' + str(words_to_use) + ', start_time: ' + str(get_time()))
    for counter, word_index in enumerate(range(0, len(all_words), skip)):
        word = all_words[word_index]
        if counter % print_every_n == 0:
            print(("(" + str(counter) + "/" + str(words_to_use) + ")").ljust(14)
                  + " Word: " + str(word).ljust(16) + " " + get_time() + ' current input size: '
                  + str(len(training_input)))#.ljust(10) + ' word_features: ' + str(word_features))
        # rhyming_words = get_rhyming_words(word)

        word_features = feature_matrix[word_to_row[word]].toarray()
        training_input.append(word_features)
    save_km_inputs(words_to_use, training_input)
    return training_input


def combine_feature_vectors_from_words(word1, word2, feature_vec1, feature_vec2, combine):
    # Note passing feature vectors to avoid having to get feature vector for word many times when performing this on
    # Note rhyming words and non rhyming words
    # NOTE now including extra features as a default
    extra_features = get_relationships(word1, word2)

    if combine:
        return np.concatenate((feature_vec1, feature_vec2, combine_features(feature_vec1, feature_vec2), extra_features), axis=1), \
               np.concatenate((feature_vec2, feature_vec1, combine_features(feature_vec1, feature_vec2), extra_features), axis=1)
    else:
        #DEBUG
        # old_way = np.concatenate((feature_vec1, feature_vec2), axis=1)
        # old_way_flatten = old_way.flatten()

        # return np.concatenate((feature_vec1.flatten(), feature_vec2.flatten(), extra_features), axis=0), \
        #        np.concatenate((feature_vec2.flatten(), feature_vec1.flatten(), extra_features), axis=0)
        #
        return np.concatenate((feature_vec1, feature_vec2, extra_features), axis=1),\
               np.concatenate((feature_vec2, feature_vec1, extra_features), axis=1)




def export_dtree_graphic(dtree, word_count, combine=True, dot_filename='dtree.dot', pdf_filename='dtree.pdf', rm_dot=True):
    dot_filename, pdf_filename = format_dtree_graphic_filenames(word_count, combine=combine, dot_filename=dot_filename, pdf_filename=pdf_filename, rm_dot=rm_dot)
    feature_names = get_feature_column_names()
    print("Exporting graphic as " + str(pdf_filename), flush=True)
    with open (dot_filename, 'w') as f:
         feature_col_names  = [name + "-left" for name in feature_names] + [name + "-right" for name in feature_names]
         if combine:
             feature_col_names = feature_col_names + [name + "-combined" for name in feature_names]
         f = tree.export_graphviz(dtree, out_file=f, feature_names=feature_col_names)
    print("Done exporting graphic, converting to pdf " + str(pdf_filename) + " , time: " + str(get_time()) + " ... ", flush=True, end='')
    subprocess.call([GRAPHIZ_DOT_LOCATION, "-Tpdf", dot_filename, "-o " + str(pdf_filename)])
    print("done")
    if rm_dot:
        os.remove(dot_filename)


def test_kmeans(km): # TODO pass feature matrix
    assert(type(km) is KMeans)
    feature_names = get_feature_column_names()
    all_words = get_all_words()
    feature_to_col = {}
    word_to_row = {}
    feature_matrix = load_feature_matrix(output=False)
    for col_num, feature in enumerate(feature_names):
        feature_to_col[feature] = col_num
    for row_num, word in enumerate(all_words):
        word_to_row[word] = row_num
    type1_errors = []  # False positive
    type2_errors = []  # False negative
    w1, w2 = get_rhyme_pairs()
    w3, w4 = get_nonrhyme_pairs()
    for index in range(len(w3)):
        res1, res2 = check_km_rhyme(km, feature_matrix, word_to_row, w3[index], w4[index], output=False)
        if res1 is True:
            type1_errors.append((w3[index], w4[index], res2))
    print("Total false positives: " + str(len(type1_errors)) + '/' + str(len(w3)) + ' = ' + str(type1_errors))
    print('  ' + str(type1_errors))
    for index in range(len(w1)):
        res1, res2 = check_km_rhyme(km, feature_matrix, word_to_row, w1[index], w2[index], output=False)
        if res1 is not True:
            type2_errors.append((w1[index], w2[index], res1, res2))
    print("Total false negatives: " + str(len(type2_errors)) + '/' + str(len(w1)) + ' = ' + str(type2_errors))
    print('  ' + str(type2_errors))

    return len(type1_errors), len(type2_errors)


def gen_training_set(feature_matrix, words_to_use, combine):
    print_every_n = 500
    feature_names = get_feature_column_names()
    # all_words = get_words_sorted()
    all_words = get_all_words()
    feature_to_col = {}
    word_to_row = {}
    row_to_word = {} # Calculate once here to save time in getting nonrhyming words
    training_input = []
    training_output = [] # Note this isn't the output of the architecture that is trained, it is the correct results for the training input
    for col_num, feature in enumerate(feature_names):
        feature_to_col[feature] = col_num
    for row_num, word in enumerate(all_words):
        word_to_row[word] = row_num
        row_to_word[row_num] = word
    #NOTE###################
    # Takes 50, 48, 49, 48, 48 seconds to do 25 words with (if rhyme in format )...
    # Try except 44, 47,47,46, 48, 47
    # Old method, which did everything via queries 1:58, 1:51, 1:52, 2:00 (create_decision_tree in nlp.py)
    #
    # so stick with try except, b/c this difference will increase w/# features, 2.5x speed!

    words_to_use = min(len(all_words), words_to_use)
    skip = int(len(all_words) / words_to_use)
    print('Generating training set, words limited to: ' + str(words_to_use) + ', combine: ' + str(combine))

    total_nonrhymes_corrected = 0  # For keeping track of improvement over categorization below

    for counter, word_index in enumerate(range(0, len(all_words), skip)):
        word = all_words[word_index]
        if counter % print_every_n == 0:
            print(("(" + str(counter) + "/" + str(words_to_use) + ")").ljust(14)
                  + " Word: " + str(word).ljust(16) + " " + get_time() + ' current input,output sizes: ('
                  + str(len(training_input)) + ',' + str(len(training_output)) + ")")#.ljust(10) + ' word_features: ' + str(word_features))
        rhyming_words = get_rhyming_words(word)
        word_features = word_feature_vector_from_matrix(feature_matrix, word_to_row, word)
        rhyming_words_added = 0 # TODO workaround for below try/except
        for rhyme in rhyming_words:
            try: # TODO improve so try is no longer needed
                rhyme_features = word_feature_vector_from_matrix(feature_matrix, word_to_row, rhyme)
                new_input1, new_input2 = combine_feature_vectors_from_words(word, rhyme, word_features, rhyme_features, combine=combine)
                training_input.append(new_input1)
                training_input.append(new_input2)
                training_output.append(1)  # They do rhyme!
                training_output.append(1)  # From the reversal
                rhyming_words_added += 1
            except:
                pass            # TODO eventually add and update these on the fly!
            # HACK doing both ways to train balanced tree
        nonrhyming_words = []
        while len(nonrhyming_words) < rhyming_words_added:
            # NOTE no longer checking that a word isnt reused. With a large enough training set this becomes not worth the time-delay

            non_rhyming_word_to_add = row_to_word[randint(0, len(all_words)-1)]
            if non_rhyming_word_to_add not in rhyming_words:
                nonrhyming_words.append(non_rhyming_word_to_add)
            else:
                total_nonrhymes_corrected += 1
        for nonrhyme in nonrhyming_words:
            # try:
            nonrhyme_features = word_feature_vector_from_matrix(feature_matrix, word_to_row, nonrhyme)
            new_input1, new_input2 = combine_feature_vectors_from_words(word, nonrhyme, word_features, nonrhyme_features, combine=combine)
            training_input.append(new_input1)
            training_input.append(new_input2)
            training_output.append(0)  # They don't rhyme!
            training_output.append(0)  # They don't rhyme!
            # except:
            #     pass            # TODO eventually add and update these on the fly!

    print("Avoided accidentally adding %s words which do rhyme as non-rhymes" % total_nonrhymes_corrected)
    save_inputs(words_to_use, training_input, training_output, combine=combine)
    # TODO return inputs, outputs


def gen_test_set(test_size):
    print_every_n = 10
    all_words = get_all_words()
    word_to_row = {} # Only necessary to fix lookup issue later
    row_to_word = {} # Calculate once here to save time in getting nonrhyming words
    for row_num, word in enumerate(all_words):
        row_to_word[row_num] = word
        word_to_row[word] = row_num

    test_size = min(len(all_words), test_size)
    print('Generating testing set, tests limited to: ' + str(test_size))
    used_indeces = dict()
    positives = [] # Words sets that rhyme
    negatives = []
    counter = 0
    while len(positives) < test_size or len(negatives) < test_size:
        first_try = True
        while first_try or word_index in used_indeces:
            word_index = random.randint(0, len(all_words))
            first_try = False
        used_indeces[word_index] = True
        word = all_words[word_index]
        if counter % print_every_n == 0:
            print(" Word: " + str(word).ljust(16) + " " + get_time() + ' current positive,negative sizes: ('
                  + str(len(positives)) + ',' + str(len(negatives)) + ")")

        rhyming_words = get_rhyming_words(word)

        # HACK ------------------------- NEED TO FIX THIS!
        rhyming_words = [word for word in rhyming_words if word in word_to_row]

        # TODO TODO need to fix get_rhyming words, by fixing the file contents, because it's returning rhyming words that aren't in feature_matrix


        if len(positives) < test_size:
            positives += [(word, rhyme_word) for rhyme_word in rhyming_words]
        if len(negatives) < test_size:
            non_rhyming_words = []
            while len(non_rhyming_words) < len(rhyming_words):
                row = random.randint(0, len(all_words) - 1) # -1 b/c randint includes endpoints
                add_word = row_to_word[row]

                # # DEBUG flaker kaaba magged causing problems
                # print("DEBUG add_word:%s, word in word_to_row:%s" % (add_word, word in word_to_row), end='')
                # if word in word_to_row:
                #     print(" Row lookup result:%s, original row result:%s" % (word_to_row[word], row))
                # else:
                #     print(' ')

                if word in word_to_row:  # TODO NEED TO FIX THIS, this is here to prevent a key error when indexing into feature_matrix, 11/11
                    if add_word not in rhyming_words and add_word not in non_rhyming_words:
                        non_rhyming_words.append(add_word)
            negatives += [(word, non_rhyme_word) for non_rhyme_word in non_rhyming_words]
        counter += 1
    positives = positives[:test_size]
    negatives = negatives[:test_size]
    save_test_set(positives, negatives)
    return positives, negatives


def save_test_set(positives, negatives):
    """
    Saves a test set for later use
    :param positives: list, Tuples of words that rhyme
    :param negatives: list, Tuples of words that don't rhyme
    :return:
    """
    count = len(positives)
    filename = format_tests_filename(count)
    pickle.dump({"positives": positives, "negatives": negatives}, open(filename, "wb"))


def load_test_set(count):
    filename = format_tests_filename(count)
    loaded = pickle.load(open(filename, "rb"))
    return loaded["positives"], loaded["negatives"]


def train_SVC(word_count=1000, SVC_filename='SVC.pickle', combine=True):
    nn_input, nn_output = load_inputs(word_count=word_count, combine=combine)
    #TODO


def load_dtree(word_count, combine, filename="dtree.pickle"):
    filename = format_dtree_filename(word_count, filename=filename, combine=combine)
    print("Loading dtree from: " + str(filename))
    return pickle.load(open(filename, "rb"))["dtree"]


def save_dtree(dtree, word_count, combine, filename="dtree.pickle"):
    filename = format_dtree_filename(word_count, filename=filename, combine=combine)
    print("Saving dtree to: " + str(filename))
    pickle.dump({"dtree": dtree}, open(filename, "wb"))


def load_forest(word_count, combine, filename='random_forest.pickle'):
    filename = format_forest_filename(word_count=word_count, combine=combine, filename=filename)
    print("Loading random forest from: " + str(filename))
    return pickle.load(open(filename, "rb"))["forest"]


def save_forest(random_forest, word_count, combine, filename='random_forest.pickle'):
    filename = format_forest_filename(word_count=word_count, combine=combine, filename=filename)
    print("Saving random forest to: %s" % filename)
    pickle.dump({"forest": random_forest}, open(filename, "wb"))


def train_dtree(word_count, combine, use_entropy, max_depth=None, min_samples_split=2, save=True, input=None, output=None):
    if input is None and output is None:
        input, output = load_inputs(word_count=word_count, combine=combine)
    if use_entropy:
        criterion = 'entropy'
    else:
        criterion = 'gini'

    dtree = tree.DecisionTreeClassifier(max_depth=max_depth, min_samples_split=min_samples_split, criterion=criterion)
    print("Training dtree combined:" + str(combine) + ', max_depth:' + str(max_depth) + ', min_samples:' + str(min_samples_split) + " " + get_time(),  end=' ... ', flush=True)
    input = np.vstack(input) # Combine the lists of arrays into a single array
    dtree = dtree.fit(input, output) # NOTE ------ THE DTREE CANNOT HANDLE CSR or LIL matrices!!!
    print("done " + get_time())
    if save:
        save_dtree(dtree, word_count=word_count, combine=combine)
    return dtree


def train_forest(word_count, combine, use_entropy, input=None, output=None, number_of_cores_to_use=3, save=True, n_estimators=20):
    # DEFAULT RFC:
    # rf = RandomForestClassifier(n_estimators=20, criterion='gini', max_depth=None, min_samples_split=2,
    #                             min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
    #                             max_leaf_nodes=None, bootstrap=True, oob_score=False,
    #                             n_jobs=1, random_state=None, verbose=0, warm_start=False, class_weight=None)
    if input is None and output is None:
        input, output = load_inputs(word_count=word_count, combine=combine)
    if use_entropy:
        criterion = 'entropy'
    else:
        criterion = 'gini'

    input = np.vstack(input)  # Combine the lists of arrays into a single array

    rf = RandomForestClassifier(n_estimators=n_estimators, criterion=criterion, max_depth=None, min_samples_split=50,
                                min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
                                max_leaf_nodes=None, bootstrap=True, oob_score=False,
                                n_jobs=number_of_cores_to_use, random_state=None, verbose=0, warm_start=False, class_weight=None)
    print("Fitting random forest using %s cores %s" % (rf.n_jobs, get_time()), end=' ... ', flush=True)
    rf.fit(input, output)
    print("done %s" % get_time())
    if save:
        save_forest(rf, word_count=word_count, combine=combine)
    return rf


def test_mla(mla, combine=True, use_manual_tests=True, test_count=None):
    if type(mla) is tree.DecisionTreeClassifier:
        print("Testing decision tree with max_depth: %s, min_samples_split: %s, criterion_is_entropy: %s" % (mla.max_depth, mla.min_samples_split, mla.criterion))
    if type(mla) is KMeans:
        print("Test mla not to be used on KMeans, use test_kmeans instead!")
        exit(1)

    feature_names = get_feature_column_names()
    all_words = get_all_words()
    feature_to_col = {}
    word_to_row = {}
    feature_matrix = load_feature_matrix(output=False)
    for col_num, feature in enumerate(feature_names):
        feature_to_col[feature] = col_num
    for row_num, word in enumerate(all_words):
        word_to_row[word] = row_num
    type1_errors = []  # False positive
    type2_errors = []  # False negative

    # NOTE for simplicity w1, w2 contain corresponding rhyming words, ie w1[0] rhymes with w2[0]
    if use_manual_tests:
        w1, w2 = get_rhyme_pairs()
        w3, w4 = get_nonrhyme_pairs()
    else:
        if type(test_count) is not int:
            raise Exception("Need to pass number of tests to run in as an int")
        positives, negatives = load_test_set(test_count)
        w1, w2 = zip(*positives)
        w3, w4 = zip(*negatives)

    rhymes_res = []


    for index in range(len(w3)): # Should all rhyme
        if index % (len(w3) / 20) == 0:
            print('.', end='', flush=True)
        res = check_rhyme(mla, feature_matrix, word_to_row, w3[index], w4[index], output=False, combine=combine)
        if res == 1:
            type1_errors.append((w3[index], w4[index]))
        rhymes_res.append(res)
    percent = len(type1_errors) / (1. * len(w3)) * 100
    print("\nTotal false positives: %s/%s (%3.2f%%) = %s" % (len(type1_errors), len(w3), percent, type1_errors))

    non_rhyme_res = []
    for index in range(len(w1)): # Should all rhyme
        if index % (len(w1) / 20) == 0:
            print('.', end='', flush=True)
        res = check_rhyme(mla, feature_matrix, word_to_row, w1[index], w2[index], output=False, combine=combine)
        if res == 0:
            type2_errors.append((w1[index], w2[index]))
        non_rhyme_res.append(res)
    percent = len(type2_errors) / (1. * len(w1)) * 100
    print("\nTotal false negatives: %s/%s (%3.2f%%) = %s" % (len(type2_errors), len(w1), percent, type2_errors))

    return len(type1_errors), len(type2_errors)


# TODO!!! note that some words have multiple pronunciation entires in cmudict
# For example, cmudict['sprinkler'] =
# [['S', 'P', 'R', 'IH1', 'NG', 'K', 'L', 'ER0'],
#  ['S', 'P', 'R', 'IH1', 'NG', 'K', 'AH0', 'L', 'ER0']]

# Note: interestingly enough, it seems that in some cases tested so far, turning combined=True, is LESS accurate than False
# Note that this was not the case with larger datasets

if __name__ == '__main__':
    word_count = 10000
    assert word_count < get_words_count()

    binary = False
    combine = True  # TODO broken within check_rhyme
    entropy = False

    generate_feature_matrix = False  # Otherwise is loaded, only needs to be done once features changed (note will also need to regen training sets)
    generate_training_set = False
    load_ml_structure = False  # Otherwise structure is generated and trained, doesn't apply when run_multiple_tests is True

    use_manual_tests = False  # If True, uses the tests manually defined in get_rhyme_pairs() and get_nonrhyme_pairs()
    generate_test_set = False  # Only applies if use_manual_tests = False, generate and save a test set of size test_count
    test_count = 10000  # Only applies if use_manual_tests = False, the number each of positive or negative tests to run

    # ML ALG:
    use_dtree = True
    use_nn = False
    use_kmeans = False
    use_random_forest = False
    # /ML ALG

    # Testing type:
    run_multiple_tests = False # If true, use method for each that runs tests with various configurations, then plots

    number_of_ml_algs_chosen = sum([use_dtree, use_kmeans, use_nn, use_random_forest])
    if not number_of_ml_algs_chosen:
        warn("No machine learning algorithm chosen!")
    elif number_of_ml_algs_chosen > 1:
        warn("More than one machine learning algorithm chosen")

    print("Running sklearn tests: word_count:%s, combine:%s, entropy:%s" % (word_count, combine, entropy))

    if generate_feature_matrix:
        feature_matrix = gen_feature_matrix(binary=binary)
    else:
        feature_matrix = load_feature_matrix(binary=binary)

    if generate_training_set and not use_kmeans:  # Kmeans has its own training set
        gen_training_set(feature_matrix, words_to_use=word_count, combine=combine)

    if not use_manual_tests and generate_test_set:
        positives, negatives = gen_test_set(test_count)

    # Use dtree
    if use_dtree:
        if not run_multiple_tests:
            if not load_ml_structure:
                dtree = train_dtree(word_count=word_count, combine=combine, use_entropy=entropy) # Autosaves
            else:
                dtree = load_dtree(word_count=word_count, combine=combine) # TODO add entripy as a criteria?
            test_mla(dtree, combine=combine, use_manual_tests=use_manual_tests, test_count=test_count)  # Non binary
        else:
            t1_error_depth = []
            t2_error_depth = []
            t1_error_min_samples = []
            t2_error_min_samples = []
            depth_range = range(55, 206, 10)
            sample_range = range(5, 1006, 100)

            dtree_input, dtree_output = load_inputs(word_count=word_count, combine=combine)

            print("\n---Testing depths:")
            for max_depth in depth_range:
                dtree = train_dtree(word_count=word_count, combine=combine, max_depth=max_depth,
                                    use_entropy=entropy, save=False, input=dtree_input, output=dtree_output)
                                    # min_samples_split=250) # HACK------------------------------------ Testing...
                t1, t2 = test_mla(dtree, combine=combine, use_manual_tests=use_manual_tests, test_count=test_count)
                t1_error_depth.append(t1)
                t2_error_depth.append(t2)
                print('')
            print("\n---Testing min_samples_split")
            for min_samples_split in sample_range:
                dtree = train_dtree(word_count=word_count, combine=combine, min_samples_split=min_samples_split,
                                    use_entropy=entropy, input=dtree_input, output=dtree_output)
                t1, t2 = test_mla(dtree, combine=combine, use_manual_tests=use_manual_tests, test_count=test_count)
                t1_error_min_samples.append(t1)
                t2_error_min_samples.append(t2)
                print('')
            import matplotlib.pyplot as plt

            plt.figure(1)
            plt.subplot(211)
            plt.plot(depth_range, t1_error_depth, 'rs-')
            plt.plot(depth_range, t2_error_depth, 'bs-')
            plt.title("max_depth FP=Red, FN=Blue")

            plt.subplot(212)
            plt.plot(sample_range, t1_error_min_samples,  'rs-')
            plt.plot(sample_range, t2_error_min_samples, 'bs-')
            plt.title("min_samples_split FP=Red, FN=Blue")

            plt.show()

        if False:
            export_dtree_graphic(dtree, word_count, combine=combine)

    # Neural net (must update sklearn to bleeding edge, better to use keras
    if use_nn:
        if run_multiple_tests:
            raise Exception("Multiple tests not yet supported for neural networks")
        # NOTE requires bleeding edge install of sklearn
        from sklearn.neural_network import MLPClassifier
        nn_input, nn_output = load_inputs(word_count=word_count, combine=combine)
        nn = MLPClassifier()
        # nn = MLPClassifier(solver='lbgfs', alpha=1e-5, hidden_layer_sizes=(100,), random_state=1)
        nn_input = np.vstack(nn_input)
        print("Training nn start: " + get_time(), end=' ... ')
        nn.fit(nn_input, nn_output)
        print("Done " + get_time())
        test_mla(nn, combine=combine, use_manual_tests=use_manual_tests, test_count=test_count)  # Non binary
    # Kmeans
    if use_kmeans:
        from sklearn import warnings
        from sklearn.utils.validation import DataConversionWarning
        warnings.simplefilter("ignore", DataConversionWarning)

        if generate_training_set: #gen km training set:
            km_input = gen_km_training_set(feature_matrix, word_count)
        else:
            km_input = load_km_inputs(word_count=word_count)

        if not run_multiple_tests:
            raise Exception("Singluar tests not supported for Kmeans")
        cluster_t1err = []
        cluster_t2err = []
        for cluster_count in range(100, 200, 5):
            km = KMeans(n_clusters=cluster_count, n_init=20)
            km_input = np.vstack(km_input)
            print("Training kmeans with n_clusters=%s, n_init=%s, starting: %s" % (km.n_clusters, km.n_init, get_time()), end='', flush=True)
            # KM.fit(km_input, km_output)
            km.fit(km_input)
            print(" done: %s" % (get_time()))

            t1err, t2err = test_kmeans(km)
            cluster_t1err.append((cluster_count, t1err))
            cluster_t2err.append((cluster_count, t2err))
        import matplotlib.pyplot as plt
        rhyme_tests = len(get_rhyme_pairs()[0]) * 1.
        non_rhyme_tests = len(get_nonrhyme_pairs()[0]) * 1.

        plt.figure(1)
        plt.subplot(211)
        plt.plot([i[0] for i in cluster_t1err], [i[1] / non_rhyme_tests for i in cluster_t1err], 'rs-')
        plt.plot([i[0] for i in cluster_t2err], [i[1] / rhyme_tests for i in cluster_t2err], 'bs-')
        plt.title("min_samples_split FP=Red, FN=Blue")
        plt.show()
    # Random forest Classifier
    if use_random_forest:
        number_of_cores_to_use = 3
        if not run_multiple_tests:
            if not load_ml_structure:
                rf = train_forest(word_count=word_count, combine=combine, use_entropy=entropy, number_of_cores_to_use=number_of_cores_to_use)  # Loads inputs & outputs
            else:
                rf = load_forest(word_count=word_count, combine=combine)
            print("Testing random forest", end='')
            if not use_manual_tests:
                print(" %s times" % test_count)
            else:
                print('')
            test_mla(rf, combine=combine, use_manual_tests=use_manual_tests, test_count=test_count)
        else:
            input, output = load_inputs(word_count=word_count, combine=combine)

            rf_t1err = []
            rf_t2err = []
            for n_estimators in range(20, 101, 5):
                # rf = RandomForestClassifier(n_estimators=n_estimators, criterion=criterion, max_depth=None, min_samples_split=50,
                #                             min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto',
                #                             max_leaf_nodes=None, bootstrap=True, oob_score=False,
                #                             n_jobs=number_of_cores_to_use, random_state=None, verbose=0, warm_start=False, class_weight=None)
                rf = train_forest(word_count=word_count, combine=combine, number_of_cores_to_use=number_of_cores_to_use, input=input, output=output,
                                  save=False, n_estimators=n_estimators)
                print("Testing random forest")
                t1_err, t2_err = test_mla(rf, combine=combine, use_manual_tests=use_manual_tests, test_count=test_count)
                rf_t1err.append((n_estimators, t1_err))
                rf_t2err.append((n_estimators, t2_err))

            import matplotlib.pyplot as plt
            rhyme_tests = len(get_rhyme_pairs()[0]) * 1.
            non_rhyme_tests = len(get_nonrhyme_pairs()[0]) * 1.
            plt.figure(1)
            plt.subplot(211)
            plt.plot([i[0] for i in rf_t1err], [i[1] * 1. / non_rhyme_tests for i in rf_t1err], 'rs-')
            plt.plot([i[0] for i in rf_t2err], [i[1] * 1. / rhyme_tests for i in rf_t2err], 'bs-')
            plt.title("min_samples_split FP=Red, FN=Blue")
            plt.show()


