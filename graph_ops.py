import networkx as nx
import matplotlib.pyplot as plt


def plot_graph(G):
    print('Resulting graph:%s' % G)
    print("Nodes: %s" % G.nodes())
    print("Edges: %s" % G.edges())

    pos = nx.spring_layout(G)  # Often doesn't work well, leads to lines through nodes etc
    # pos = nx.circular_layout(graph)
    # pos = nx.shell_layout(graph)
    # pos = nx.spectral_layout(graph)
    labels = {}
    for node_num, node in enumerate(G.nodes()):
        labels[node] = node
    nx.draw_networkx_nodes(G, pos, node_size=600)
    nx.draw_networkx_labels(G, pos, labels, font_size=16)
    nx.draw_networkx_edges(G, pos)
    plt.tight_layout()
    plt.show()

def process_graph(G):
    # Reprocessing graph
    # Want to find articulation points, start by removing nodes who only have one connection
    art_degrees = nx.degree(G)
    art_degrees = list(art_degrees.items())
    for node, degree in art_degrees:
        if degree < 2:
            print("Removing node:%s for having degree:%s" % (node, degree))
            G.remove_node(node)

    art_clusters = nx.clustering(G)
    nodes_with_low_cc = [(node, cluster_value) for node, cluster_value in art_clusters.items() if
                         0 < cluster_value <= .5]  # Note ignoring nodes with = 0, which have 0 or 1 connections
    print("Removing nodes with low clustering coef: %s" % sorted(nodes_with_low_cc, key=lambda tup: tup[1]))
    for node in nodes_with_low_cc:
        G.remove_node(node[0])

    articulation_points = nx.articulation_points(G)
    articulation_points = [i for i in articulation_points]
    print("Removing articulation points of art graph:%s" % articulation_points)
    for ap in articulation_points:
        G.remove_node(ap)

        # bicomponent_graphs = [i for i in nx.biconnected_component_subgraphs(art_graph)] # TODO this could still be relevant and useful
        # print("Bicompont graphs:")
        # for bigraph in bicomponent_graphs:
        #     print(bigraph.nodes())
    return G