from rappy_config import *
import mysql.connector
from util import *
from random import randint
import ast
import nltk
import glob
import os

####### GLOBALS
sql_cursor = None
sql_cnx = None
error_outputs = []
error_commands = []


def execute(query, values, output=True, errors=OUTPUT_FROM_EXECUTE_ON_ERROR, ignore_errnos =[]):
    '''
    Executes an SQL query on the server
    :param query: Query string, with '%(value)s' terms where appropriate
    :param values: Dict with entries corresponding to the %(keys)s above
    :param output: True if the execution prints its success
    :return:
    '''
    shownvalues = dict(values)

    if output:
        print("Executing: {} ... ".format(query % shownvalues), end="", flush=True)
    try:
        sql_cursor.execute(query, values)
    except mysql.connector.Error as err:
        if err.errno in ignore_errnos:
            print("Success*")  # * to indicate that an error has been ignored
        else:
            if errors or output:
                if not output: # Need to display the query issues
                    print("Executing: {} ... ".format(query % shownvalues), end="", flush=True)
                print("ERROR\nMySQL Error: {}\n".format(err), flush=True)
                error_outputs.append(err)
                error_commands.append('{}'.format(query % shownvalues))

    else:
        if output:
            print("Success")


def wipe_database(require_confirmation=True, output=True):
    if require_confirmation:
        warn("You are about to erase the entire database (%s), are you sure? (y/n)" % (DB_NAME))
        user_input = input(">")
    if not require_confirmation or user_input in 'yY':
        execute("CREATE DATABASE IF NOT EXISTS %s;" % DB_NAME, [], output=output)
        execute("USE %s;" % DB_NAME, [], output=output)
        execute("DROP TABLE IF EXISTS WordOccurances", [], output=output)
        execute("DROP TABLE IF EXISTS Words", [], output=output)
        execute("DROP TABLE IF EXISTS SongArtists", [], output=output)
        execute("DROP TABLE IF EXISTS Songs", [], output=output)
        execute("DROP TABLE IF EXISTS Artists", [], output=output)
        execute("DROP TABLE IF EXISTS Rhymes", [], output=output)
        print("Completed wiping of database, restablishing...")
        set_up_database()
    else:
        print("Database left as is")


def set_up_database():
    print("Establishing DB")
    execute("CREATE DATABASE IF NOT EXISTS %s;" % (DB_NAME), [], output=False)
    execute("USE %s;" % DB_NAME, [], output=False)

    execute("CREATE TABLE IF NOT EXISTS Words("
            "word CHAR(35) UNIQUE,"
            "PRIMARY KEY(word));", [], output=False)

    execute("CREATE TABLE IF NOT EXISTS WordOccurances("
            "word CHAR(25)," # Used to keep track of specific uses of words
            "line_no DECIMAL(3,0) NOT NULL, "
            "line_index DECIMAL(13,0) NOT NULL, "
            "song_id DECIMAL(3,0) NOT NULL, " # Use 0 to indicate has no songid (not from a song)
            "PRIMARY KEY(line_no, line_index, song_id),"
            "FOREIGN KEY(word) REFERENCES Words(word));", [], output=False)


    execute("CREATE TABLE IF NOT EXISTS Songs("
            "id DECIMAL(3,0)," # uid is the unique id used to identify a song
            "name CHAR(50) NOT NULL,"
            "PRIMARY KEY(id));", [], output=False)

    execute("CREATE TABLE IF NOT EXISTS Artists("
            "name CHAR(25),"
            "PRIMARY KEY (name));", [], output=False)

    execute("CREATE TABLE IF NOT EXISTS SongArtists("
            "name CHAR(25), "
            "song_id DECIMAL(3,0), "
            "PRIMARY KEY (song_id), "
            "FOREIGN KEY (name) REFERENCES Artists(name), "
            "FOREIGN KEY (song_id) REFERENCES Songs(id));", [], output=False)
            # Used to model the many-many relationship between Songs and Artists
            # For each artist a song has, there is an entry with the Song's songid and the Artist's name

    execute("CREATE TABLE IF NOT EXISTS Rhymes("
            "word1 CHAR(25) NOT NULL, "
            "word2 CHAR(25) NOT NULL);", [], output=False)


def save_database(output=True):
    if output:
        print("Saving DB...", end='')
    sql_cnx.commit()
    if output:
        print('Saved', end='\n', flush=True)


def close_database():
    sql_cursor.close()
    sql_cnx.close()
    print("Closed DB")


def get_next_songid():
    '''
    :return: The next unused unique song id (an integer), starting at 1
    '''
    execute('SELECT COUNT(*) FROM Songs', [], output=False)
    results = sql_cursor.fetchall()
    return results[0][0] + 1


def add_word(text, output=False):
    '''
    This adds words that aren't directly from songs, and instead have been chosen because of their frequency of use in the english language.
    Eventually we will look for rhymes for these same words
    :param word:
    :return:
    '''

    # TODO maintain a column for whether the word has been scraped for matches (because this may take some stop-start)
    exec_str = 'INSERT IGNORE INTO Words VALUES(%(word_text)s)'
    values = {
        'word_text': text,
    }
    execute(exec_str, values, output=output)


def add_lyric(lyric):
    print("Adding Lyric: " + lyric.short_str())


    # TODO=======================================================
    # Check not in DB
    # Find any SongArtists with Artists name, each has a songid
    # Check those ID's against


    mysongid = None
    for artist in lyric.artists:
        if lyric.title in songs_by_artist(artist):
            mysongid = songid_of_song_artists(lyric.title, artist)
            print("Found existing songid for this lyric: " + str(mysongid) + ' NOT ADDING LYRIC')

    if mysongid is None:
        mysongid = get_next_songid()

        # Updating Songs table
        values = {
            'name': lyric.title,
            'id': mysongid
        }
        exec_str = 'INSERT IGNORE INTO Songs VALUES (%(id)s, %(name)s)'
        execute(exec_str, values, output=False)

        # Add artists to Artists and SongArtists
        for artist in lyric.artists:

            exec_str = 'INSERT IGNORE INTO Artists VALUES (%(name)s)'
            values['name'] = clean_text(artist)
            execute(exec_str, values, output=False)

            # Artist is now in Artists table, need to add to SongArtists

            values = {
                'name': clean_text(artist),
                'songid': mysongid
            }
            exec_str = 'INSERT IGNORE INTO SongArtists VALUES (%(name)s, %(songid)s)'
            execute(exec_str, values, output=False)

        # Finally add the words from the song to the DB
        # First word must exist in Words, before can be added to WordOccurances

        exec_str = 'INSERT IGNORE INTO WordOccurances VALUES(%(word_text)s, %(line_no)s, %(line_index)s, %(songid)s)'
        for (line_no, line_index), word in lyric.word_dict.items():
            word.text = clean_text(word.text) # Redundant cleaning to fix words left in saved lyrics
            add_word(word.text, output=False)

            values = {
                'word_text': word.text,
                'line_no': line_no,
                'line_index': line_index,
                'songid': mysongid
            }
            execute(exec_str, values, output=False) # Adding to WordOccurances

        # TODO add rhymes!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


def show_mysql_errors():
    if not len(error_outputs):
        print("\nThere were no MYSQL errors this execution :)")
    else:
        print("\nThere were " + str(len(error_outputs)) + ' MYSQL errors this execution:')
        for err_no in range(len(error_outputs)):  # To err is human, to for-give divine
            print(' ' + str(err_no) + ') ' + str(error_commands[err_no]))
            print('    ' + str(error_outputs[err_no]))


def add_rhyme(word1, word2, output=True):
    '''
    Adds a rhyme between words 1 and 2 to MYSQL DB. Words do NOT need to be ordered when supplied
    :param word1:
    :param word2:
    :return:
    '''

    if word2 < word1:
        buf = word1
        word1 = word2
        word2 = buf
    execute("INSERT IGNORE INTO Rhymes VALUES (%(word1)s, %(word2)s)", {'word1': word1, 'word2' : word2}, output=output)


def aritst_in_artists_table(name):
    exec_str = "SELECT * FROM Artists WHERE name='" + name + '\''
    execute(exec_str, [])
    # Sanity check
    count = sum(1 for name in sql_cursor)
    if count > 1:
        warn("Warning, artist name: " + str(name) + ' in Artists table ' + str(count) + ' times')
    if count:
        return True
    else:
        return False


def songsids_by_artist(artist):
    exec_str = "SELECT song_id FROM SongArtists WHERE name='" + artist + '\''
    execute(exec_str, [], output=False)
    return [int(song_id[0]) for song_id in sql_cursor] # Cast to int b/c comes back as a decimal


def songs_by_artist(artist):
    exec_str = "SELECT Songs.name " \
               "FROM SongArtists " \
               "INNER JOIN Songs " \
               "ON songartists.song_id=Songs.id " \
               "WHERE SongArtists.name='" + artist + '\';'
    execute(exec_str, [], output=False)
    return [song_name[0] for song_name in sql_cursor]


def songid_of_song_artists(song_name, artist):
    exec_str = "SELECT Songs.id FROM SongArtists " \
               "INNER JOIN Songs " \
               "ON songartists.song_id=Songs.id " \
               "WHERE SongArtists.name='" + artist + "\' " \
               "AND Songs.name='" + song_name + "\'"
    execute(exec_str, [], output=False)
    ids = [id for id in sql_cursor]
    if len(ids) > 1:
        warn("Possible duplicate with multipe song ids: " + str(ids))
    if len(ids):
        return ids[0][0]
    else:
        return []


def add_column_to_table(table_name, column_name, column_type='TINYINT', output=True):
    if not column_in_table(table_name, column_name):
        execute("ALTER TABLE " + str(table_name) + " ADD `" + str(column_name) + "` " + column_type, [], output=output)


def add_feature_column_to_words(column_name, feature_type, column_type='TINYINT', output=True):
    # if not column_in_table('Words', column_name):
    full_column_name = "f_" + str(feature_type) + '_' + column_name  # TODO TOODODODODODODODO------------------------------------------
    if not column_in_table('Words', full_column_name, output=False):
        execute("ALTER TABLE Words ADD `" + full_column_name + '` ' + column_type, [], output=output, ignore_errnos=[1060])  # HACK ignore duplicate column


def get_words_count():
    exec_str = 'SELECT COUNT(*) FROM Words'
    execute(exec_str, [], output=False)
    return sql_cursor.fetchall()[0][0]


def column_in_table(tablename, column_name, output=False):
    execute(
            'SELECT NULL FROM information_schema.COLUMNS '
            'WHERE table_name = \'' + tablename + '\' '
            'AND table_schema = \'' + str(DB_NAME) + '\' '
            'AND COLUMN_NAME = "' + column_name + '"', [], output=output)
            # NOTE using ` instead of ' for column_name causes error..
    buf = sql_cursor.fetchone()
    return buf is not None


def get_column_names(tablename):
    execute("SELECT COLUMN_NAME FROM information_schema.COLUMNS "
            "WHERE table_name = '" + str(tablename) + "' "
            "AND table_schema = '" + str(DB_NAME) + "'",[], output=False)
    return [column_name[0] for column_name in sql_cursor]


def get_word_features():
    columns = get_column_names('Words')
    return [column[2:] for column in columns if 'f_' == column[:2]]


def get_all_words(output=False):
    execute("SELECT word FROM Words", [], output=output)
    return [word[0] for word in sql_cursor]


def get_words_sorted(sort_by="v_frequency_corpora", output=False):
    execute("SELECT word FROM Words ORDER BY " + sort_by + ' DESC', [], output=output)
    return [word[0] for word in sql_cursor]


def get_rhyming_words(word, use_file=True, output=False):
    filename = SCRAPED_FILES_DIR + word + '.html'
    with open(filename, 'rb') as f:
        buf = f.read().decode("utf-8")
        f.close()
    buf = ast.literal_eval(buf)
    ret = []
    for item in buf:
        word_to_add = item["word"]
        if " " not in word_to_add:
            ret.append(word_to_add)
    ret.append(word)  # Added 11/10/16 make sure that a word always rhymes with itself!
    return ret


def update_word(word, column_name, newval, output=False):
    if type(newval) == str:
        # Need quotes around newval
        execute("UPDATE Words SET `" + str(column_name) + "`=\"" + str(newval) +
                "\" WHERE word=\"" + word + "\"", [], output=output)
    else:
        # Cannot have quotes around newval
        execute("UPDATE Words SET `" + str(column_name) + "`=" + str(newval) +
                " WHERE word=\"" + word + "\"", [], output=output)


def add_word_counts(word_corpora='Corpora/count_1w.txt'):
    print("Adding word counts for all words in corpora: " + str(word_corpora))
    add_column_to_table('Words', 'v_frequency_corpora', column_type='DECIMAL(12,0)')

    num_lines = sum(1 for line in open(word_corpora))
    print_every_n = 50000
    with open(word_corpora, 'rb') as f:
        for line_no, line in enumerate(f):
            if line_no % print_every_n == 0:
                print("Adding count for word #" + str(line_no) + '/' + str(num_lines))
            line = line.decode("utf-8")
            word = line[:line.index('\t')]
            word_freq = ast.literal_eval(line[line.index('\t')+1:])
            update_word(word, 'v_frequency_corpora', word_freq, output=False)
    save_database()


def add_cmudict_info_for_all_words(): # Note only does words in cmudict
    print("Adding cmudict representation for all words")
    add_column_to_table('Words', 'v_cmurep', column_type='CHAR(100)') # v_ to indicate a varied version of the word representing the row
    all_words = get_all_words()
    word_count = len(all_words)
    print_every_n = 25000
    cmudict = nltk.corpus.cmudict.dict()

    for index, word in enumerate(all_words):
        if index % print_every_n == 0:
            print(' Adding cmurrep of word #' + str(index) + ' / ' + str(word_count) + ' = ' + str(word) + ' : ' + str(cmudict[word][0]))
        update_word(word, 'v_cmurep', '_'.join(cmudict[word][0]), output=False)
    save_database()


def get_feature_column_names():
    execute("SELECT column_name FROM information_schema.COLUMNS WHERE table_name = 'Words' AND table_schema = '%s'" % DB_NAME,
            [], output=False)
    feature_cols = []
    for row in sql_cursor:
        if row[0][:2] == 'f_':
            feature_cols.append(row[0])
    return feature_cols


def load_feature_vectors():
    feature_names = get_feature_column_names()
    exec_str = "SELECT "
    for feature in feature_names:
        exec_str += '`' + feature + '`, '
    exec_str = exec_str[:-2] + " FROM Words"
    execute(exec_str, [], output=False)
    # return [row for row in sql_cursor] # NOTE Don't combine, too large of a data structure.


def get_feature_vector_for_word(word):
    feature_names = get_feature_column_names()
    exec_str = "SELECT "
    for feature in feature_names:
        exec_str += "`" + feature + '`, '
    exec_str = exec_str[:-2] + " FROM Words WHERE word=\"" + str(word) + "\""
    execute(exec_str, [], output=False)
    return sql_cursor.fetchone()


# NOTE: Below are cursor operations, providng an easier way to interact with the cursor than direct MYSQL queries and parsing
# First load the cursor with whatever you need, then keep asking for the next item


def load_cursor_with_words(output=False):
    exec_str = 'SELECT Word FROM Words'
    execute(exec_str, [], output=output)


def next_item_from_cursor():
    buf = sql_cursor.fetchone()
    if buf is not None:
        return buf[0]
    return None


def rows_in_cursor():
    return sql_cursor.rowcount


def remove_word(word, output=False):
    # HACK
    if word[0] == '%':
        word = '\\' + word

    execute('DELETE FROM Words WHERE word="' + str(word) + '"', [], output=output)
    # execute('DELETE FROM Words WHERE word=\'' + str(word) + '\'', [], output=output)
    # execute('DELETE FROM Words WHERE word=`' + str(word) + '`', [], output=output)


def remove_words_with_no_rhymes(update=True, output=False):
    no_rhymes = get_words_with_no_rhymes()
    print("Removing %s words with no rhymes: " % (len(no_rhymes)))
    if update is True:
        update = 1000
    elif update is False:  # Allows setting of update by passing an integer
        update = 0
    for index, word in enumerate(no_rhymes):
        if update > 0 and index % update == 0:
            print(" Removing word " + str(index) + " / " + str(len(no_rhymes)))
        remove_word(word, output=output)
    save_database()
    print("Now removing corresponding files and database entries ... ", end='')
    words_without_files = []
    for word in no_rhymes:
        filename = "scraped/" + str(word) + ".html"
        if os.path.isfile(filename):
            os.remove(filename)
        else:
            words_without_files.append(word)
    print("done")
    print("Couldn't find scraped file for words: " + str(words_without_files))


def get_words_with_no_rhymes(): # Technically this isn't MYSQL, but it makes sense here
    # Note assuming that this is faster than checking rhyme for each word from rhyme table...
    print("Getting words with no rhymes ... ", end='')

    # TODO have to check each word in table to make sure it has an entry, and that the entry is populated.
    no_rhymes = []
    all_words = get_all_words()

    for word in all_words:
        filename = SCRAPED_FILES_DIR + word + '.html'
        if os.path.isfile(filename):
            with open(filename, 'rb') as f:
                if f.read().decode("utf-8") == '[]':
                    no_rhymes.append(word)
                f.close()
        else:
            no_rhymes.append(word)
    print("done (" + str(len(no_rhymes)) + ")")
    return no_rhymes
    # no_rhymes = []
    # files = glob.glob(SCRAPED_FILES_DIR + "*.html")
    # for filename in files:
    #     # TODO have to confirm that word in db!!!
    #     with open(filename, 'rb') as f:
    #         buf = f.read().decode("utf-8")
    #         if buf == '[]':
    #             no_rhymes.append(filename[8:-5])
    #         f.close()
    # print("done (" + str(len(no_rhymes)) + ")")
    # return no_rhymes


#############################################

def get_nth_row_of_words(rownum):
    '''
    Rows start at 0
    :param rownum:
    :return:
    '''
    execute("SELECT * FROM Words ORDER BY word LIMIT " + str(rownum) + ", 1", [], output=False)
    return sql_cursor.fetchone()

def get_nth_word(rownum):
    '''
    Rows start at 0

    :param rownum:
    :return:
    '''
    execute("SELECT word FROM Words ORDER BY word LIMIT " + str(rownum) + ", 1", [], output=False)
    return sql_cursor.fetchone()[0]


def get_n_nonrhyming_words(word, count):
    rhyming_words = get_rhyming_words(word)
    nonrhyming_words = []
    total_words = get_words_count()
    # print("Number of rhyming words: " + str(len(rhyming_words)))
    while len(nonrhyming_words) < count:
        row_num = randint(0, total_words)
        non_rhyming_word = get_nth_word(row_num)
        if non_rhyming_word not in rhyming_words:
            nonrhyming_words.append(non_rhyming_word)
    return nonrhyming_words


############################################################


# INFO Connecting to the MYSQL server here
try:
  print("Connecting to server ... ", end='', flush=True)
  sql_cnx = mysql.connector.connect(**mysql_config)
  sql_cursor = sql_cnx.cursor()
  print("done, using database: %s" % DB_NAME, flush=True)

  execute("CREATE DATABASE IF NOT EXISTS %s;" % DB_NAME, [], output=False)
  execute("USE %s;" % DB_NAME, [], output=False)



except mysql.connector.Error as err:
  print("Connection Error: {}".format(err))
  sys.exit(1)
