This project is currently very much in progress

![Construction Sign](http://images.mysafetysign.com/img/lg/S/under-construction-caution-sign-s-0816.png)

# Goals:
* Automatically label the rhyme scheme in rap lyrics
    * Use natural language processing and machine learning to classify words as rhymes 
* Rank lyrics based on complexity and lyrics
    * Use natural language processing to determine semantic relations between words 
    * Look for plays on words, contradictions and so on

# Project State:
* Messy but functional
    * rappy.py contains methods to scrape a database to ./Scraped, and fill in a MYSQL database (config in config.py)
        * Recently switched to [CMU-Dict](http://www.speech.cs.cmu.edu/cgi-bin/cmudict) for word database and for pronunciation; away from metaphone/double-metaphone, which caused problems due to their over-compression
    * sklearn_tests.py contains ways to train and test decision trees with different characteristics. Includes a test set. Currently ~90% accuracy on words that do rhyme, ~60% accuracy on words that don't

# Upcoming changes / TODOs
* Filter out words from CMU-Dict that have no rhymes (currently scraping from api.datamuse.com, perhaps switch sources?)
* Move away from simple decision trees - while easy to interpret, they have significant limitations - currently reviewing random forests & SVMs

# Interface (Blazing Arrow by Blackalicious):
![Blazing Arrow](https://www.dropbox.com/s/4ax7f2wtyehiegj/Interface%208_31%20Blazing%20Arrow.png?dl=1)
Youtube audio link:

[![Source song](http://img.youtube.com/vi/FdrossKXGb4/0.jpg)](http://www.youtube.com/watch?v=FdrossKXGb4 "Blazing Arrow")

Example of Rap analysis (12 mins), best example @ 7:05:

[![Rapping Deconstructed](http://img.youtube.com/vi/QWveXdj6oZU/0.jpg)](http://www.youtube.com/watch?v=QWveXdj6oZU "Rapping, deconstructed: The best rhymers of all time")