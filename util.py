from rappy_config import *
from time import strftime
import os

def clean_text(text):
    '''
    Cleans a single isolated word's string representation, to a unified format
    :param text: The word to be cleaned
    :return: A str representing the cleaned word
    '''
    text = ''.join([char for char in text if char in 'ABCDEFGHIJKLNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-\''])
    return text.lower()


def with_commas(number):
    '''
    Formats a number as a string, with commas
    :param number: The (non-string) number to be formatted
    :return: A formatting string representing the number with commas
    '''
    return "{:,}".format(number)


def warn(string):
    print('\n>\n->\n--> WARNING: ' + str(string) + ' <--\n->\n>', flush=True)


def get_time():
    '''
    Returns the current, local Hour:Minute:Second as a string
    :return: str(Hour:Minute:Second)
    '''
    return strftime("%H:%M:%S")


def format_dtree_graphic_filenames(word_count, combine=True, dot_filename='dtree.dot', pdf_filename='dtree.pdf'):
    initial_dot_filename = dot_filename
    initial_pdf_filename = pdf_filename

    dot_suffix_index = initial_dot_filename.rfind('.')
    pdf_suffix_index = initial_pdf_filename.rfind('.')

    if dot_suffix_index != -1:
        dot_filename = initial_dot_filename[:dot_suffix_index]
    if pdf_suffix_index != -1:
        pdf_filename = initial_pdf_filename[:pdf_suffix_index]
    dot_filename += '_' + str(word_count)
    pdf_filename += '_' + str(word_count)
    if combine:
        dot_filename += '_combined'
        pdf_filename += '_combined'
    if dot_suffix_index != -1:
        dot_filename += initial_dot_filename[dot_suffix_index:]
        pdf_filename += initial_pdf_filename[pdf_suffix_index:]
    return dot_filename, pdf_filename


def format_dtree_filename(word_count, combine, filename="dtree.pickle"):
    base_filename = PICKLE_DIR + filename
    suffix_pos = base_filename.rfind(".")
    if suffix_pos != -1:
        filename = base_filename[:suffix_pos]
        filename += "_" + str(word_count)
    if combine:
            filename = filename + "_combined"
    if suffix_pos != -1:
        filename = filename + base_filename[suffix_pos:]
    return filename


def format_forest_filename(word_count, combine, filename='forest.pickle'):
    base_filename = PICKLE_DIR + filename
    suffix_pos = base_filename.rfind(".")
    if suffix_pos != -1:
        filename = base_filename[:suffix_pos]
        filename += "_" + str(word_count)
    if combine:
            filename = filename + "_combined"
    if suffix_pos != -1:
        filename = filename + base_filename[suffix_pos:]
    return filename


def format_dtree_training_filename(word_count, filename="dtree_ins_outs.pickle", combine=True):
    base_filename = PICKLE_DIR + filename
    suffix_pos = base_filename.rfind(".")
    if suffix_pos != -1:
        filename = base_filename[:suffix_pos]
    filename += "_" + str(word_count)
    if combine:
        filename += "_combined"
    if suffix_pos != -1:
        filename = filename + base_filename[suffix_pos:]
    return filename


def format_matrix_filename(filename="feature_matrix.pickle", binary=False):
    filename = PICKLE_DIR + filename
    if binary:
        suffix_index = filename.find(".pickle")
        filename = filename[:suffix_index] + '_binary' + filename[suffix_index:]
    return filename


def format_pythondb_filename(filename="pythondb.pickle"):
    filename = PICKLE_DIR + filename
    suffix_index = filename.find(".pickle")
    return filename[:suffix_index] + "_" + str(DB_NAME) + filename[suffix_index:]


def format_tests_filename(count, filename='ml_tests.pickle'):
    filename = PICKLE_DIR + filename
    suffix_index = filename.find(".pickle")
    suffix = filename[suffix_index:]
    return filename[:suffix_index] + '_' + str(DB_NAME) + '_' + str(count) + suffix


def establish_directory_structure():
    necessary_dirs = [PICKLE_DIR, 'logs', 'lyrics', 'scraped']
    for directory in necessary_dirs:
        if not os.path.exists(directory):
            os.makedirs(directory)

