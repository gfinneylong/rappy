from tkinter import *
from functools import partial
import pickle
import datetime
import os.path
import platform

from sql_interface import *
from scraper import *
from nlp import *
from ml import *
import copy
import networkx as nx
from graph_ops import plot_graph, process_graph

class Interface:

    # colors = ["red", "green", "blue", "cyan", "yellow", "magenta"]
    colors = ['peach puff', 'lemon chiffon', 'lavender', 'dark slate gray', 'cornflower blue',
              'slate blue', 'dodger blue', 'deep sky blue', 'sea green', 'dark olive green', 'spring green', 'green',
              'yellow', 'gold', 'indian red', 'tan', 'orange', 'magenta', 'purple']


    # # Need to check if running Linx or Windows, OSX will never be supported for Tkinter
    if platform.system() == 'Linux':
        default_color = 'gray'
    elif platform.system() == 'Windows':
        default_color = "SystemButtonFace"

    default_font = ("Arial", 10)
    bold_font = ("Arial", 10, "bold")
    current_interface = None  # Used as 'self' within events (like clicks)

    def __init__(self, lyric):
        """
        Generates an interactive window for tagging/viewing lyrics
        :param lyric: The lyric to display via this interface
        :return:
        """
        # print("Creating interface from lyric:\n" + str(lyric))
        self.lyric = lyric
        Interface.current_interface = self
        self.color_index = 0
        self.cur_color = Interface.colors[self.color_index]
        self.buttons = dict()
        self.button_colors = dict()
        self.buttons_per_color = [0] * len(Interface.colors)
        self.last_button = None

        self.window = Tk()
        self.window.wm_title("Blazing arrow by Blackalicious")
        self.window.bind('<Button-1>', Interface.any_left_click)
        self.window.bind('<Button-2>', Interface.any_middle_click)
        self.window.bind('<Button-3>', Interface.any_right_click)
        self.canvas = Canvas(self.window, width=200, height=100)
        self.canvas.pack()
        self.set_title()

        ordered_words = self.lyric.get_words_ordered()
        for word in ordered_words:
            new_button = Button(self.canvas, text=word.text + ', ' + str(word.cmu), bg=Interface.default_color, font=Interface.default_font)
            # new_button = Button(self.canvas, text=word.text + ', ' + str(word.dm), bg=Interface.default_color, font=Interface.default_font)
            if len(word.groups):
                # Probably from a load, this word already has some colors associated with it
                new_button.config(bg=Interface.colors[min(word.groups)])
                if len(word.groups) > 1:
                    new_button.config(font=Interface.bold_font)
                for val in word.groups:
                    self.buttons_per_color[val] += 1
            self.buttons[word.line_no, word.line_index] = new_button
            self.button_colors[word.line_no, word.line_index] = word.groups
            new_button.bind('<Button-1>', partial(Interface.left_click_button, word.line_no, word.line_index))
            new_button.grid(row=word.line_no, column=word.line_index)
        mainloop()

        # Now save the lyric's relations
        for r_c, color_values in self.button_colors.items():
            self.lyric.word_dict[r_c].groups = color_values
        self.lyric.save()

    def any_left_click(event):
        Interface.current_interface.last_button = '<Button-1>'
        # This is called whenever there is any left click within the window
        pass

    def any_right_click(event):
        # Use this to change colors, thus indicating that a group of rhymes is complete! (All the same color)
        # Only advance such that there's one color that has no buttons with that color (to start a new pattern)
        # Otherwise loop back to already used color (stops user from switching through empty colors)

        current_interface = Interface.current_interface
        current_interface.last_button = '<Button-3>'
        # print("Any right click, current index: " + str(current_interface.color_index) + " count of that color: " + str(Interface.colors[current_interface.color_index]))

        first_zero_index = -1
        first_nonzero_index = -1
        next_nonzero_index = -1

        current_index = current_interface.color_index
        for index, val in enumerate(current_interface.buttons_per_color):
            if index != current_interface.color_index:
                if val == 0:
                    if first_zero_index == -1:
                        first_zero_index = index
                else:
                    if first_nonzero_index == -1:
                        first_nonzero_index = index
                    if index > current_index and next_nonzero_index == -1:
                        next_nonzero_index = index
        # print("First zero index: " + str(first_zero_index) + ' = ' + str(Interface.colors[first_zero_index]))

        if current_interface.buttons_per_color[current_interface.color_index] == 0:
            # First search to the end, then double back around
            if next_nonzero_index == -1:
                # Nothing ahead, so wrap!
                if first_nonzero_index == 1:
                    # No colors anywhere!
                    current_interface.set_index(0)
                else:
                    # More colors, they're just behind the current_index, so wrap
                    if first_nonzero_index == -1:
                        # Could be no first non-zero index, in which case we just switch to the first color
                        current_interface.set_index(0)
                    else:
                        current_interface.set_index(first_nonzero_index)
            else:
                current_interface.set_index(next_nonzero_index)
        else:
            # Strategy: We always want the earliest zero to be used as the new color
            # So if that earliest zero is AFTER the current index and BEFORE the next non-zero color
            # Then we move forwards to(wards) that color
            # Otherwise we will eventually loop back to it, and so should instead choose the next non-zero element if it exists
            # If it doesnt, then we loop back to the beginning, (Element 0, which is either count 0 or a utilized color)
            if first_zero_index == -1 or first_zero_index > current_interface.color_index:
                # No zeros in colors (Maybe add more colors here later?)
                # OR
                # Have not encountered any 0's in root of sequence, so need to utilize one from the tail
                # OR
                # First zero is behind, so jump to the next_nonzero_index if available (this case is unavailable)
                current_interface.offset_index()
            else:
                if next_nonzero_index == -1:
                    # => no more used colors ahead
                    # => go to root
                    next_nonzero_index = 0
                # otherwise move to the next non_zero_index
                current_interface.set_index(next_nonzero_index)

        current_interface.update_title()
        for key, values in current_interface.button_colors.items():
            if current_interface.color_index in values:
                current_interface.buttons[key].config(bg=current_interface.cur_color)

    def any_middle_click(event):
        if Interface.current_interface.last_button == '<Button-2>':
            Interface.current_interface.window.destory()
        else:
            print("Middle click again to save the current rhyme scheme")
        Interface.current_interface.last_button = '<Button-2>'

    def left_click_button(row, col, event):
        # print("Left clicked button, current color_index: " + str(Interface.current_interface.color_index))
        if Interface.current_interface.color_index in Interface.current_interface.button_colors[row, col]:
            # Button has already been marked with cur_color, so remove that marking
            Interface.current_interface.button_colors[row, col].remove(Interface.current_interface.color_index)
            Interface.current_interface.buttons_per_color[Interface.current_interface.color_index] -= 1
            if len(Interface.current_interface.button_colors[row, col]):
                Interface.current_interface.buttons[row, col].config(bg=Interface.colors[ min(Interface.current_interface.button_colors[row, col])])
            else:
                print("Resetting button to default color")
                Interface.current_interface.buttons[row, col].config(bg=Interface.default_color)
        else:
            # Button has not been colorec with the current color, so color
            Interface.current_interface.buttons_per_color[Interface.current_interface.color_index] += 1
            Interface.current_interface.buttons[row, col].config(bg=Interface.current_interface.cur_color)
            Interface.current_interface.button_colors[row, col].append(Interface.current_interface.color_index)

        if len(Interface.current_interface.button_colors[row, col]) > 1:
            Interface.current_interface.buttons[row, col].config(font=Interface.bold_font)
        else:
            Interface.current_interface.buttons[row, col].config(font=Interface.default_font)

    def right_click_button(row, col, event):
        pass

    def get_button_text(button):
        return button.config('text')[-1]

    def get_button_color(button):
        return button.config('bg')[-1]

    def offset_index(self, offset=1):
        """
        Offset the interface's color_index by an offset from the current index. Index will wrap the ends
        Also updates the interace's cur_color, corresponding to its color_index
        :param offset: The amount to increase the interface's color_index by (relative to current index)
        """
        self.color_index += 1
        self.color_index %= len(Interface.colors)
        self.cur_color = Interface.colors[self.color_index % len(Interface.colors)]

    def set_index(self, new_index):
        """
        Update the interface's color_index to a new index. Wraps and warns on too large a new_index
        Also updates the interace's cur_color, corresponding to its color_index
        :param new_index: The new value of the interface's color_index
        """
        if new_index >= len(Interface.colors):
            print("\nWARNING applying excessive color_index: " + str(new_index))
        self.color_index = new_index % len(Interface.colors)
        self.cur_color = Interface.colors[self.color_index % len(Interface.colors)]

    def set_title(self):
        self.title = self.lyric.title + ' by '
        if len(self.lyric.artists) > 1:
            self.title += '( '
            for index, artist in enumerate(self.lyric.artists):
                if not index:
                    self.title += ' + '
                self.title += artist
            self.title += ' )'
        else:
            self.title += self.lyric.artists[0]
        self.update_title()

    def update_title(self):
        # Now include current color
        self.window.wm_title(self.title + ' -- ' + self.cur_color )

class Lyric:
    def __init__(self, artists, song_name, lines, run_interface=True):
        assert(type(artists) is list)
        self.artists = artists # The PRIMARY artist is the first one supplied.
        self.title = song_name
        self.lines = lines
        self.word_dict = {}  # Maps (r,c) to word

        for line_no, line in enumerate(lines):
            line = line.split()
            for word_no, word in enumerate(line):
                self.word_dict[line_no, word_no] = Word(clean_text(word), line_no, word_no)
        # Now construct GUI for labeling

        self.cur_traits = {}  # Traits which may be kept updated (like words since the last rhyme)
        if run_interface:
            self.run_interface()

        # Interface closed, assign traits to words


    def setup_lyric_traits(self):
        self.rhyme_groups = []  # Each group is a group where all words rhyme with one another.
                                # A instance of a word can be present in multiple groups
                                # Different instances of the same word can be in the same group

    def save(self, base_dir='./lyrics/'):
        pickle_dict = {}
        pickle_dict['lyric'] = self
        pickle_dict['time_saved'] = datetime.datetime
        filename = self.title + '_by_' + self.artists[0]
        print("Saving to: " + str(base_dir + filename))
        pickle.dump(pickle_dict, open(base_dir + filename, "wb"))

    def __str__(self):
        res = '---' + self.short_str() + ' ---\n'

        for index, line in enumerate(self.lines):
            last_index = len(self.lines) - 1
            if index == last_index:
                res += line
            else:
                res += line + '\n'
        return res

    def short_str(self):
        res = ''
        if len(self.artists) == 1:
            res += self.title + ' by ' + str(self.artists[0])
        else:
            res += self.title + ' by ' + str(self.artists)
        return res

    def get_words_ordered(self):
        return [index_word[1] for index_word in sorted(self.word_dict.items())]

    __repr__ = __str__

    def run_interface(self):
        interface = Interface(self)
        # Interface has closed, all rhyme have been identified. Now time to store this information, for later calculations (train a rhyme classifier)


    def tag_with_mla(self, mla):
        # IDEA HERE:
        # Rather than compare each word to every other, compare just against 2 lines above, and 2 lines below
        # This will require the use of graph algorithms..

        print("------\nRunning Experimental new tag with mla")
        feature_matrix = load_feature_matrix(binary=False, output=True)
        all_words = get_all_words()
        word_to_row = {}
        for row_num, word in enumerate(all_words):
            word_to_row[word] = row_num

        sorted_dict_items = sorted(list(self.word_dict.items()))

        number_of_rows = max(row for (row, col) in self.word_dict.keys())
        line_to_words = [[] for i in range(number_of_rows + 1)]

        for (row, col), word in self.word_dict.items():
                line_to_words[row].append(word)

        graph = nx.Graph()
        graph.add_nodes_from(list(word.text for word in self.word_dict.values()))


        line_range_to_check = 2 # TODO improve this
        rhyme_sets = set()


        for row_num1, row1 in enumerate(line_to_words):
            # for row_num2 in range(row_num1, row_num1 + line_range_to_check + 1):
            for row_num2 in range(row_num1, len(line_to_words)):
                if row_num2 < len(line_to_words):
                    row2 = line_to_words[row_num2]
                    for word1 in row1:
                        for word2 in row2:
                            if word1 != word2:
                                try:
                                    res = check_rhyme(mla, feature_matrix, word_to_row, word1.text, word2.text, combine=True, output=False)
                                    if res:
                                        print("----Found rhyme between %s & %s" % (word1, word2))
                                        graph.add_edge(word1.text, word2.text)
                                except:
                                    print("Skipped finding rhyme between %s & %s" % (word1, word2))

        # print("\n\nGraph Analysis:")
        # clustering = nx.clustering(graph)
        # connected_components = [i for i in nx.connected_components(graph)]
        # degrees = nx.degree(graph)
        # print(" Clustering:")
        # for cluster in clustering.items():
        #     print("  " + str(cluster))
        # print(" Connected Components:%s")
        # for cc in connected_components:
        #     print("  " + str(cc))
        # print(" Degree:%s")
        # for d in degrees.items():
        #     print("  " + str(d))

        processed_graph = process_graph(graph)
        plot_graph(processed_graph)

        # connected_components = [i for i in nx.connected_components(processed_graph) if len(i) > 1]
        connected_components = [i for i in nx.connected_components(processed_graph)]
        # TODO maybe count how many times a word appears, and if its in a group of 1, allow that group only if the word appears multiple times

        for ((row, col), word) in sorted_dict_items:
            for index, rhyme_set in enumerate(connected_components):
                # print("Checking if %s in %s" % (word, rhyme_set))
                if word.text in rhyme_set:
                    word.groups.add(index)
                    break



    def old_tag_with_mla(self, mla):
        # NOTE
        # NOTE
        # NOTE THIS IS OUTDATED, KEEPING FOR RERFERENCE AND USEFUL SNIPPETS
        # NOTE
        # NOTE

        current_group = 0
        feature_matrix = load_feature_matrix(binary=False, output=True)
        all_words = get_all_words()
        word_to_row = {}
        for row_num, word in enumerate(all_words):
            word_to_row[word] = row_num
        rhymes_found = 0
        sorted_dict_items = sorted(list(self.word_dict.items()))

        # NOTE the following code is highly dependent on a word rhyming with itself
        rhyme_sets = set()

        # for index1, ((row1, col1), word1) in enumerate(sorted_dict_items):

        def filter_criteria(set_in, otherset):
            '''
            True if set_in is a subset of otherset
            '''
            print("   Checking if %s is not a subset and not a superset of %s" % (set_in, otherset))
            print("    subset: %s" % set_in.issubset(otherset))
            print("    superset: %s" % set_in.issuperset(otherset))
            print("    res:%s" % set_in.issubset(otherset) or set_in.issuperset(otherset))
            # return True #set_in.issubset(otherset) or set_in.issuperset(otherset)
            return not (set_in.issubset(otherset) or set_in.issuperset(otherset)) # BEST SO FAR

        index1 = 0

        while index1 < len(sorted_dict_items):
            ((row1, col1), word1) = sorted_dict_items[index1]

            if word1.text in all_words:
                print("Processing word:%s, %s" % (word1, get_time()))
                rhymes_with_word = set([word1.text])

                # DEBUG--------------------------------------------------------------
                # for index2, ((row2, col2), word2) in enumerate(sorted_dict_items[index1:]): # TODO ideally revert to this
                for index2, ((row2, col2), word2) in enumerate(sorted_dict_items):
                # DEBUG--------------------------------------------------------------

                    if row1 != row2 or col1 != col2:
                        if word2.text in all_words:
                            if check_rhyme(mla, feature_matrix, word_to_row, word1.text, word2.text, combine=True, output=False):
                                rhymes_with_word.add(word2.text)
                    # index2 += 1

                # Now have a set of words that rhyme with current word, need to eliminate incrementally the word that rhymes the least with other_words
                print("All words that rhyme with \'%s\':%s" % (word1.text, rhymes_with_word))

                # rhymes_per_word = dict()
                # for word in rhymes_with_word:
                #     rhymes_per_word[word] = sum(
                #         check_rhyme(mla, feature_matrix, word_to_row, word, word2, combine=True, output=False) for word2 in rhymes_with_word)
                # highest_rhyme_count = max(rhymes_per_word.values())
                # rhymes_with_word = frozenset([word for word, count in rhymes_per_word.items() if
                #                     count[0] != highest_rhyme_count])  # Note switching from set to list

                # NOTE: different approach, b/c test:{'west', 'sat', 'fat', 'best', 'rat', 'flat', 'not', 'cat', 'rest', 'test'}, reduced to {}
                rhymes_with_word = frozenset(rhymes_with_word)

                # print("  Reduced list of words that rhyme with %s:%s" % (word1.text, rhymes_with_word))
                if len(rhymes_with_word) > 1:
                    # rhymes_with_word = set(rhymes_with_word)

                    if rhymes_with_word in rhyme_sets:
                        print('--------- rhymes with word already in rhyme_sets: %s' % rhymes_with_word)
                    else:
                        is_subset = False
                        for index3, rhyme_list in enumerate(rhyme_sets):
                            if rhymes_with_word.issubset(rhyme_list):
                                print('-- NOTE -- (not adding) %s is a subset of rhyme_list %s:%s' % (rhymes_with_word, index3, rhyme_list))
                                print('-- NOTE -- ^ between sets:%s' % (rhyme_list ^ rhymes_with_word))
                                is_subset = True
                            # if rhyme_list.issubset(rhymes_with_word):
                            #     print("** removing pre-existing set %s for being subset of %s" % (rhyme_list, rhymes_with_word))
                            #     rhyme_sets.remove(rhyme_list) # TODO make sure works correctly with iterator
                            #     index3 -= 1
                            # index3 += 1

                        if not is_subset:
                            # NOTE THIS IS SUPPOSED TO BE A REPLACEMENT FOR THE ABOVE CODE, which checks if rhyme_list is a subset of rhymes_with_word
                            rhmye_sets_before_filter = copy.deepcopy(rhyme_sets) # DEBUG
                            # lambda filter: http://stackoverflow.com/questions/7045754/python-list-filtering-with-arguments
                            print(" ORIGINAL SETS:%s" % rhmye_sets_before_filter)

                            # NOTE a filter -------KEEPS------- when a condition is true
                            rhyme_sets = set(filter(lambda x: filter_criteria(x, rhyme_list), rhyme_sets)) # keeps eachs set within rhyme_sets if that set is not a subset of rhymes_with_word
                            if len(rhmye_sets_before_filter) != len(rhyme_sets):
                                print("Removed pre-existing sets")
                                # print(" Original sets:%s" % rhmye_sets_before_filter)
                                # print(" Current sets:%s" % rhymes_with_word)
                                # print(" Removed sets:%s" % (rhmye_sets_before_filter - rhyme_sets))

                            rhyme_sets.add(rhymes_with_word)
                            print("RHYME SETS AFTER ADDING CURRENT RHYMES: %s" % rhyme_sets)


            index1 += 1


        print("All rhyme_sets:%s" % rhyme_sets)

        groups = dict()

        for ((row, col), word) in sorted_dict_items:
            for rhyme_index, rhyme_set in enumerate(rhyme_sets):
                # print("Checking if %s in %s" % (word, rhyme_set))
                if word.text in rhyme_set:
                    word.groups.add(rhyme_index)


        for group_num, group in enumerate(rhyme_sets):
            print("Words in group #%s" % group_num, end=': ')
            for index, ((row, col), word) in enumerate(sorted_dict_items):
                if group_num in word.groups:
                    print(word.text, end=', ')
            print('', end='\n')

        # NOTE ALTERNATIVE TO ABOVE
        print("PRINTING GROUPS WHEN STROED AS SETS")
        for index, group_set in enumerate(rhyme_sets):
            print("%s:%s" % (index, group_set))


class Word:
    """
    A word within a lyric, NOT an abstraction of a word without a specific context
    """

    def __init__(self, text, line_no, line_index):
        self.text = text

        self.cmu = None
        if self.text in cmudict:
            self.cmu = cmudict[self.text][0]

        self.line_no = line_no
        self.line_index = line_index
        self.groups = set()
        self.traits = {} # These are assigned when the window closes. Words with similar traits but from different lyrics are not related

    def __str__(self):
        return '(' + self.text + ', ' + \
               '[' + str(self.line_no) + ',' + str(self.line_index) + '], ' \
               + str(self.cmu) + ')'

    __repr__ = __str__


class Rhyme:
    def __init__(self, lyric, words):
        self.words = words
        self.lyric = lyric


def load_lyric(filename, base_dir='lyrics/'):
    pickle_dict = pickle.load(open(base_dir + filename, "rb"))
    return pickle_dict['lyric']


def process_cmudict_words():
    print("Adding cmu words")
    cmudict = nltk.corpus.cmudict.dict()
    skipped_words = []
    print_every_n = int(len(cmudict) / 5)

    non_allowed_chars = '\'!"#%&(),-./:;?%{}.' # TODO may want to make an exception for "'"
    for index, word in enumerate(cmudict.keys()):
        if index % print_every_n == 0:
            print(" Adding word #" + str(index) + ' / ' + str(len(cmudict)) + ' = ' + str(word))
        if not any(char in word for char in non_allowed_chars):
            add_word(word)
        else:
            skipped_words.append(word)
    print('Didn\'t add the following ' + str(len(skipped_words)) + ' words: ' + str(skipped_words) + ' due to containing one of the following chars: ' + non_allowed_chars)
    save_database()


if __name__ == '__main__':
    establish_directory_structure()
    if WIPE_DATA:
        wipe_database(require_confirmation=True, output=False) #Also sets up db
    else:
        set_up_database()

    if SCRAPE_RHYME_DATA:
        scrape_from_datamuse() # Gets rhyme files for all words in cmudict, and saves to Scraped/

    if POPULATE_WORDS_AND_REPS_AND_RHYMES:
        process_cmudict_words() # Adds cmu words to words table
        add_cmudict_info_for_all_words() # Adds cmu reps to words table
        process_data_from_datamuse() # Adds rhymes

    if REMOVE_WORDS_WITH_NO_RHYMES:
        remove_words_with_no_rhymes(update=1000, output=False)

    if ADD_NORMAL_NGRAMS:
        my_tree = make_tree_all_words(max_length=MAX_STD_NGRAM_LEN)
        my_tree.add_n_most_common_ngrams(STD_NGRAM_COUNTS, min_length=MIN_STD_NGRAM_LEN, offset=0, column_type='ngram', seperator='_')

    if ADD_CMUDIT_NGRAMS:
        cmu_tree = make_tree_all_cmudict(max_length=MAX_CMU_NGRAM_LEN)
        cmu_tree.add_n_most_common_ngrams(CMU_NGRAM_COUNTS, min_length=MIN_CMU_NGRAM_LEN, offset=0, column_type='ngramcmu', seperator='_')

    if RUN_INTERFACE:
        if LOAD_INTERFACE:
            blazing_arrow = load_lyric("Blazing Arrow_by_Blackalicious")
            if hasattr(blazing_arrow, 'authors'):    # HACK to fix legacy
                blazing_arrow.artists = blazing_arrow.authors

            blazing_arrow.run_interface()
            # add_lyric(blazing_arrow)

            save_database()
            close_database()
        else:
            if True: # HACK
                blazing_arrow = Lyric(["Blackalicious"], "Blazing Arrow", [
                "Amazin phase your days your hazy ways my Blazing Arrow",
                "The rays that range from Asia way to Rio De Janeiro",
                "A craze you crave? but work today's the day that in a major way",
                "I'll make you say oh look he saved the day and also paved the way (hooray)",
                "Rockin? my crew like razor blades today and days away from now okay, okay",
                "I'm like a laser ray keepin the stains? away",
                "and dangerous ways rappin the phrase that pays",
                "all things just ain't a phase but ageless ways a cagey great displays",
                "yo hey, this ain't a game, so stays awake",
                "cause if you came to play you'll all behave insane",  # 10
                "After we change the game it won't reamin the same",
                "(I'll fade away!!!) So make your disc and play this tape in your Camaro",
                "Amazin phase your days your hazy ways my Blazing Arrow",

                "The verbal God, I throw a murder all my foe and kill 'em",
                "But learnin all and so I'm gorowing while I'm learnin wit em",
                "Infernal I'll explode returnin out the globe about to blow",
                "Now get up out here yo, see root on out the zones that I patrol",
                "Rock and roll might tear you out the soul like blunts and alcohol",
                "I knock your whole toboggan for a loop and now the doubt will flow",
                "I'm not the pro you wanna knock cause on the real I got the glow",  # 20
                "Cosmic flows as I suppose thats how its supposed to be",
                "and got to go!!! How'd ya know? Intuition",
                "I was on the old safe surface figure out your purpose that's impossible",
                "But logic will disturb the thought or focus what its not is all about",
                "the grow about the kind of onus only god can know",
                "I rock it for the chocolate, for the awkward for the thoughtless",
                "in your home or at your office",
                "I'm your early morning coffee so who got the ball?",
                "We roll with Navajos with double barrels",
                "Amazin phase your days your hazy ways my Blazing Arrow"], run_interface=False)  # 30

                dtree = load_dtree(word_count=5000, combine=True)
                blazing_arrow.tag_with_mla(dtree)
                blazing_arrow.run_interface()
            else:
                # Using this to experiment with a smaller lyric
                test_lyric = Lyric(["Test"], "Lyric_name",
                                   [
                                    "Test the best lyric in the west",
                                    "Rest not for the best I guess",
                                    "Toy boy khoi leroy",
                                    "he she me bee tree ski flee",
                                    "The fat cat sat on the flat rat",
                                    "A crazy haze of rap phrases with amaze your days away"
                                   ], run_interface=False)

                # dtree = load_dtree(word_count=10000, combine=True)
                mla = load_forest(word_count=5000, combine=True)

                test_lyric.old_tag_with_mla(mla)
                test_lyric.run_interface()

    show_mysql_errors()

